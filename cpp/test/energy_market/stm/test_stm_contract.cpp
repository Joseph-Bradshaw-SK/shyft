#include <doctest/doctest.h>

#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <test/energy_market/serialize_loop.h>

using namespace shyft::energy_market::stm;
using std::make_shared;

TEST_SUITE("stm_contract") {

    TEST_CASE("contract_construct") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto ctr=make_shared<contract>(1,"ctr","{}", sys);
        sys->contracts.push_back(ctr);
        auto h= make_shared<stm_hps>(1,"hps");
        sys->hps.push_back(h);
        stm_hps_builder b(h);
        auto u =b.create_unit(1,"u1","{}");
        auto p =b.create_power_plant(1,"p1","{}");
        power_plant::add_unit(p,u);
        ctr->power_plants.push_back(p);
        FAST_CHECK_UNARY(ctr);
        FAST_CHECK_EQ(ctr->id,1);
        FAST_CHECK_EQ(ctr->name,"ctr");
        FAST_CHECK_EQ(ctr->json,"{}");
        FAST_REQUIRE_EQ(ctr->power_plants.size(),1);
        FAST_CHECK_EQ( *p, *ctr->power_plants[0]);
        ctr->tsm["x"]=apoint_ts{};
        auto o=test::serialize_loop(sys);
        FAST_CHECK_EQ(o->contracts[0]->power_plants.size(),1);
        FAST_CHECK_EQ(*o, *sys);
    }
    TEST_CASE("contract_market_association") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto mkt=make_shared<energy_market_area>(1,"ema","{}", sys);
        auto ctr=make_shared<contract>(1,"c1","{}", sys);
        FAST_CHECK_EQ(sys->market.size(), 0);
        FAST_CHECK_EQ(sys->contracts.size(), 0);
        FAST_CHECK_EQ(mkt->contracts.size(), 0);
        FAST_CHECK_EQ(ctr->get_energy_market_areas().size(), 0);
        sys->market.push_back(mkt);
        sys->contracts.push_back(ctr);
        mkt->contracts.push_back(ctr);
        FAST_CHECK_EQ(sys->market.size(), 1);
        FAST_CHECK_EQ(sys->contracts.size(), 1);
        FAST_CHECK_EQ(mkt->contracts.size(), 1);
        FAST_CHECK_EQ(ctr->get_energy_market_areas().size(), 1);
        mkt->contracts.clear();
        FAST_CHECK_EQ(mkt->contracts.size(), 0);
        FAST_CHECK_EQ(ctr->get_energy_market_areas().size(), 0);
        ctr->add_to_energy_market_area(mkt);
        FAST_CHECK_EQ(mkt->contracts.size(), 1);
        FAST_CHECK_EQ(ctr->get_energy_market_areas().size(), 1);
    }
    TEST_CASE("contract_portfolio_association") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto cpf=make_shared<contract_portfolio>(1,"cp","{}", sys);
        auto ctr=make_shared<contract>(1,"c1","{}", sys);
        FAST_CHECK_EQ(sys->contract_portfolios.size(), 0);
        FAST_CHECK_EQ(sys->contracts.size(), 0);
        FAST_CHECK_EQ(cpf->contracts.size(), 0);
        FAST_CHECK_EQ(ctr->get_portfolios().size(), 0);
        sys->contract_portfolios.push_back(cpf);
        sys->contracts.push_back(ctr);
        cpf->contracts.push_back(ctr);
        FAST_CHECK_EQ(sys->contract_portfolios.size(), 1);
        FAST_CHECK_EQ(sys->contracts.size(), 1);
        FAST_CHECK_EQ(cpf->contracts.size(), 1);
        FAST_CHECK_EQ(ctr->get_portfolios().size(), 1);
        cpf->contracts.clear();
        FAST_CHECK_EQ(cpf->contracts.size(), 0);
        FAST_CHECK_EQ(ctr->get_portfolios().size(), 0);
        ctr->add_to_portfolio(cpf);
        FAST_CHECK_EQ(cpf->contracts.size(), 1);
        FAST_CHECK_EQ(ctr->get_portfolios().size(), 1);
    }
}
