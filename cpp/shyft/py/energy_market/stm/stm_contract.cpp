/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_tsm_expose.h>

#include <boost/format.hpp>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::time_series::dd::apoint_ts;
    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::core::calendar;
    using shyft::time_axis::generic_dt;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;

    using boost::format;

    template<> string str_(stm::contract const& o) {
        return (format("Contract(id=%1%, name='%2%')")
            %o.id
            %o.name
        ).str();
    }

    template<> string str_(stm::contract_portfolio const& o) {
        return (format("ContractPortfolio(id=%1%, name='%2%')")
            %o.id
            %o.name
        ).str();
    }

    void stm_contract() {
        auto c=py::class_<
            stm::contract,
            py::bases<>,
            shared_ptr<stm::contract>,
            boost::noncopyable
        >("Contract",
          doc_intro("A contract between two parties, seller and buyer, for sale of energy at a given price."),
          py::no_init);
        c
            .def(py::init<int, const string&, const string&, stm::stm_system_&>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("sys")),
                "Create contract with unique id and name for a stm system."))
            .def_readwrite("id",&stm::contract::id,"Unique id for this object.")
            .def_readwrite("name",&stm::contract::name,"Name for this object.")
            .def_readwrite("json",&stm::contract::json,"Json keeping any extra data for this object.")
            .add_property("tag", +[](const stm::contract& self){return url_tag(self);}, "Url tag.")
            .def_readonly("power_plants",&stm::contract::power_plants,"List of associated power plants.")
            .def(py::self==py::self)
            .def(py::self!=py::self)
            .def("flattened_attributes", +[](stm::contract& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
            .def("get_portfolios", &stm::contract::get_portfolios, "Get any portfolios this contract is associated with. Convenience for search in ContractPortfolio.contracts.")
            .def("add_to_portfolio", &stm::contract::add_to_portfolio, "Add this contract to specified portfolio. Convenience for appending to ContractPortfolio.contracts.")
            .def("get_market_areas", &stm::contract::get_energy_market_areas, "Get any energy market areas this contract is associated with. Convenience for search in MarketArea.contracts.")
            .def("add_to_market_area", &stm::contract::add_to_energy_market_area, "Add this contract to specified energy market area. Convenience for appending to MarketArea.contracts.")
        ;
        expose_str_repr(c);
        expose_tsm(c);
        add_proxy_property(c,"quantity", stm::contract, quantity, "[J/s] Contract quantity, in rate units, so that integrated over contract period gives total volume.")
        add_proxy_property(c,"price", stm::contract, price, "[money/J] Contract price.")
        add_proxy_property(c,"fee", stm::contract, fee, "[money/s] Any contract fees, in rate units so that fee over the contract period gives total fee.")
        add_proxy_property(c,"revenue", stm::contract, revenue, "[money/s] Calculated revenue, in rate units, so that integrated of the contract period gives total revenue volume")
        add_proxy_property(c,"parent_id", stm::contract, parent_id, "Optional reference to parent (contract). Typically for forwardes/futures, that is splitted into shorter terms as time for the delivery is approaching.")
        add_proxy_property(c,"active", stm::contract, active, "Contract status (dead/alive).")
        add_proxy_property(c,"validation", stm::contract, validation, "Validation status (and whether the contract has been validated or not).")
        add_proxy_property(c,"buyer", stm::contract,buyer,"The name of the buyer party of the contract.")
        add_proxy_property(c,"seller", stm::contract,seller,"The name of the seller party of the contract.")

        expose_vector_eq<stm::contract_>("ContractList", "A strongly typed list of Contract.", &stm::equal_attribute<std::vector<stm::contract_>>, false);

        auto cp=py::class_<
            stm::contract_portfolio,
            py::bases<>,
            shared_ptr<stm::contract_portfolio>,
            boost::noncopyable
        >("ContractPortfolio",
          doc_intro("Stm contract portfolio represents a set of contracts, so that the sum of interesting contract properties can be evaluated and compared."),
          py::no_init);
        cp
            .def(py::init<int, const string&, const string&, stm::stm_system_&>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("sys")),
                "Create contract portfolio with unique id and name for a stm system."))
            .def_readwrite("id",&stm::contract_portfolio::id,"Unique id for this object.")
            .def_readwrite("name",&stm::contract_portfolio::name,"Name for this object.")
            .def_readwrite("json",&stm::contract_portfolio::json,"Json keeping any extra data for this object.")
            .add_property("tag", +[](const stm::contract_portfolio& self){return url_tag(self);}, "Url tag.")
            .def_readonly("contracts",&stm::contract_portfolio::contracts,"List of contracts.")
            .def(py::self==py::self)
            .def(py::self!=py::self)
            .def("flattened_attributes", +[](stm::contract_portfolio& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
        ;
        expose_str_repr(cp);
        expose_tsm(cp);
        add_proxy_property(cp,"quantity", stm::contract_portfolio, quantity, "[J/s] normally sum of contracts, unit depends on the contracts.")
        //add_proxy_property(c,"price", stm::contract_portfolio, price, "[money] Price of the portfolio, normally average over contracts.")
        add_proxy_property(cp,"fee", stm::contract_portfolio, fee, "[money/s] Fees of the portfolio, normally sum of contracts.")
        add_proxy_property(cp,"revenue", stm::contract_portfolio, revenue, "[money/s] Calculated revenue.")

        expose_vector_eq<stm::contract_portfolio_>("ContractPortfolioList", "A strongly typed list of ContractPortfolio.", &stm::equal_attribute<std::vector<stm::contract_portfolio_>>, false);
    }
}
