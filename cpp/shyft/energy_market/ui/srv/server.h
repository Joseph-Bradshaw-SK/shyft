#pragma once
#include <shyft/srv/server.h>
#include <shyft/srv/db.h>
#include <shyft/srv/model_info.h>
#include <shyft/srv/msg_defs.h>
#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/core/core_archive.h>
#include <functional>
#include <stdexcept>
#include <memory>
#include <iostream>
#include <sstream>

namespace shyft::energy_market::ui::srv {
    using shyft::srv::server;
    using shyft::srv::db;
    using shyft::srv::message_type;
    using shyft::srv::msg;
    using shyft::srv::model_info;
    using shyft::core::utctime_now;
    using shyft::core::utcperiod;
    using shyft::core::core_arch_flags;
    using std::string;
    using std::runtime_error;

    using std::shared_ptr;
    using fx_read_cb_t = std::function<string(const string&, const string&)>;

    /** @brief db-storage for layout info with callback functionality.
     *
     * This class is responsible for storing layouts to a backing-store
     * file-system, along with model-info, that contains useful, shorthand
     * information about the models.
     *
     * When calling config_db::read_model, the database will first attempt
     * to read an already stored layout from the file-system.
     * If it is unable to retrieve a layout with the provided ID,
     * it will try to generate the layout from the provided arguments
     * using the callback function, if present.
     */
    struct config_db: public db<layout_info> {
        using super = db<layout_info>;
        fx_read_cb_t read_cb; // Callback function to read a layout if not present yet.read a layout if not present yet.

        config_db(string const& root_dir): super(root_dir) {}

        /** @brief Read layout with additional arguments and name if layout is not present in file-system.
         * If it uses the callback function, the newly generated layout will be stored in the file-system
         * with either provided ID, or first valid ID on server.
         *
         * @param mid: Layout ID
         * @param layout_name: Name of new layout if it needs to be generated from callback
         * @param args: json-string with additional arguments to the callback function to generate new layout
         * @return shared_ptr<layout_info>: Either stored layout_info or a new, generated one.
         */
        shared_ptr<layout_info> read_model_with_args(int64_t mid, const string& layout_name, const string& args, bool store_layout) {
            shared_ptr<layout_info> li;
            // 1. Try to read a model, using the standar approach
            try {
                li = super::read_model(mid);
            } catch (const runtime_error& /*err*/) {
                // 2. If this didn't work, some grammar is needed to tell how to generate
                // new layout info
                string layout = read_cb ? read_cb(layout_name, args) : "";
                if (layout.empty()) {
                    throw runtime_error("read_model: Did not find layout, and unable to generate new");
                } else {
                    int64_t rmid = mid; // Realized model ID
                    if (store_layout) {
                        if (mid <= 0) {
                            rmid = mk_unique_model_id();
                        }
                    } else {
                        rmid = -1;
                    }
                    li = std::make_shared<layout_info> (rmid, layout_name, layout);
                    model_info mi(rmid, layout_name, utctime_now(), "");
                    if (store_layout)
                        super::store_model(li, mi);
                }
            }
            return li;
        }

        string read_model_blob_with_args(int64_t mid, const string& layout_name, const string& args, bool store_layout) {
            string li_blob;
            try {
                // 1. Try to read a model already in store:
                li_blob = super::read_model_blob(mid);
            } catch (const runtime_error& /*err*/) {
                // 2. We need to try to generate a new layout
                string layout = read_cb ? read_cb(layout_name, args) : "";
                if (layout.empty()) {
                    throw runtime_error("read_model_blob: Did not find layout, and unable to generate a new");
                } else {
                    int64_t rmid = mid; // Realized model ID
                    if (store_layout) {
                        if (mid <= 0) {
                            rmid = mk_unique_model_id();
                        }
                    } else { // We set layout_ID = -1 to signal that a layout is generated, but not stored
                        rmid = -1;
                    }
                    auto li = std::make_shared<layout_info>(rmid, layout_name, layout);
                    model_info mi(rmid, layout_name, utctime_now(), "");
                    if (store_layout) {
                        super::store_model(li,mi);
                    }
                    // Generate blob:
                    std::stringstream ss;
                    {
                        shyft::core::core_oarchive oa(ss, core_arch_flags);
                        oa << li;
                    }
                    li_blob = ss.str();
                }
            }
            return li_blob;
        }
    };

    struct config_server: server<config_db> {
        using super = server<config_db>;

        config_server(string root_dir): super(root_dir) {}
        config_server(config_server&&)=delete;
        config_server(const config_server&)=delete;
        config_server& operator=(const config_server&)=delete;
        config_server& operator=(config_server&&)=delete;
        ~config_server() = default;

        void set_read_cb(fx_read_cb_t const& fx) { db.read_cb = fx; }
        fx_read_cb_t get_read_cb() const { return db.read_cb; }

    protected:
        virtual bool message_dispatch(
            std::istream & in,
            std::ostream & out,
            message_type::type msg_type
            )
        {
            using shyft::core::core_iarchive;
            using shyft::core::core_oarchive;
            using shyft::core::core_arch_flags;
            if (!super::message_dispatch(in,out,msg_type)) { // Additional message types not in base class
                switch(msg_type) {
                    case message_type::MODEL_READ_ARGS: {
                        core_iarchive ia(in, core_arch_flags);
                        int64_t mid;
                        string layout_name, args;
                        bool store_layout;
                        ia >> mid >> layout_name >> args >> store_layout;
                        auto result = db.read_model_blob_with_args(mid, layout_name, args, store_layout);
                        msg::write_type(message_type::MODEL_READ_ARGS, out); // then send
                        out.write(result.data(), result.size());
                    } break;
                    // Other
                    default:
                        return false;
                }
            }
            return true;
        }

    };
}
