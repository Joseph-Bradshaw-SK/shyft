import pytest
from functools import reduce
from shyft.energy_market.stm import StmSystem, Busbar, Network, TransmissionLine

def test_transmission_line_attributes():
    sys = StmSystem(1, "A", "{}")
    n = Network(2, 'Network', '{}', sys)
    sys.networks.append(n)
    t = TransmissionLine(3, 'TransmissionLine #1', '{}', n)
    n.transmission_lines.append(t)
    # generics, from id_base
    assert hasattr(t, "id")
    assert hasattr(t, "name")
    assert hasattr(t, "json")
    # specifics
    assert hasattr(t, "capacity")
    assert t.tag == "/n2/t3"

def test_transmission_line_attributes_flattened():
    sys = StmSystem(1, "A", "{}")
    n = Network(2, 'Network', '{}', sys)
    sys.networks.append(n)
    t = TransmissionLine(3, 'TransmissionLine #1', '{}', n)
    n.transmission_lines.append(t)
    assert t.flattened_attributes()
    for (path, val) in t.flattened_attributes().items():
        attr = reduce(getattr, path.split('.'), t)
        assert type(val) is type(attr)
        assert val == attr
        assert val.url() == attr.url()

def test_transmission_line_association():
    sys = StmSystem(1, "A", "{}")
    n = Network(2, 'Network', '{}', sys)
    sys.networks.append(n)
    t = TransmissionLine(3, 'TransmissionLine #1', '{}', n)
    n.transmission_lines.append(t)
    b1 = Busbar(3, 'Busbar #1', '{}', n)
    b2 = Busbar(4, 'Busbar #2', '{}', n)
    n.busbars.extend([b1, b2])
    b1.add_to_start_of_transmission_line(t)
    b2.add_to_end_of_transmission_line(t)
    assert t.from_bb == b1
    assert t.to_bb == b2

def test_transmission_line_equal():
    sys = StmSystem(1, "A", "{}")
    n = Network(2, 'Network', '{}', sys)
    sys.networks.append(n)
    t = TransmissionLine(3, 'TransmissionLine #1', '{}', n)
    t_2 = TransmissionLine(3, 'TransmissionLine #1', '{}', n)
    t_3 = TransmissionLine(4, 'hmm', '{}', n)
    n.transmission_lines.extend([t, t_2, t_3])

    assert t == t
    assert t == t_2
    assert t != t_3
    # Equal busbar associations
    b1 = Busbar(4, 'Busbar #1', '{}', n)
    b1_2 = Busbar(4, 'Busbar #1', '{}', n)
    n.busbars.extend([b1, b1_2])
    b1.add_to_start_of_transmission_line(t)
    assert t != t_2
    b1_2.add_to_start_of_transmission_line(t_2)
    assert t == t_2