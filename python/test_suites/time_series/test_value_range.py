import numpy as np
import pytest
from shyft.time_series import (Calendar, TimeAxis, TimeSeries, TsVector,
                               UtcPeriod, point_interpretation_policy)


def test_value_range():
    """"
    Added here for reference (c++ tests covers the algorithms) 
    """
    cal = Calendar()
    t0 = cal.time(2019, 1, 1)
    pointfx_avg = point_interpretation_policy.POINT_AVERAGE_VALUE

    # extreme situation: only 1 value: min == max
    ts = TimeSeries(TimeAxis(t0, cal.HOUR, 1), [1], pointfx_avg)
    tsv = TsVector([ts])
    ask_period = UtcPeriod(t0 + 0.5 * cal.HOUR, t0 + 5.5 * cal.HOUR)
    min_ts, max_ts = tsv.value_range(ask_period)
    assert min_ts == pytest.approx(1.)
    assert max_ts == pytest.approx(1.)

    # outside range
    ts = TimeSeries(TimeAxis(t0, cal.HOUR, 1), [1], pointfx_avg)
    tsv = TsVector([ts])
    ask_period = UtcPeriod(t0 + 1.5 * cal.HOUR, t0 + 5.5 * cal.HOUR)
    min_ts, max_ts = tsv.value_range(ask_period)
    assert np.isnan(min_ts)
    assert np.isnan(max_ts)

    # only-nan input
    ts = TimeSeries(TimeAxis(t0, cal.HOUR, 1), [np.nan], pointfx_avg)
    tsv = TsVector([ts])
    ask_period = UtcPeriod(t0 + 1.5 * cal.HOUR, t0 + 5.5 * cal.HOUR)
    min_ts, max_ts = tsv.value_range(ask_period)
    assert np.isnan(min_ts)
    assert np.isnan(max_ts)

    # ts(v) with more than 1 value; period aranged with dt
    ts = TimeSeries(TimeAxis(t0, cal.HOUR, 5), [1.0, 2, 3, 4, np.nan], pointfx_avg)
    tsv = TsVector([ts])
    ask_period = UtcPeriod(t0 + cal.HOUR, t0 + 6 * cal.HOUR)
    min_ts, max_ts = tsv.value_range(ask_period)
    assert min_ts == pytest.approx(2.)
    assert max_ts == pytest.approx(4.)

    # shift ask_period-start by dt/2
    min_ts, max_ts = tsv.value_range(ask_period)
    assert min_ts == pytest.approx(2.)
    assert max_ts == pytest.approx(4.)

    # tsv with several entries
    ts2 = TimeSeries(TimeAxis(t0, cal.HOUR, 4), [1.0, 2, 6, 4], pointfx_avg)
    tsv = TsVector([ts, ts2])
    min_ts, max_ts = tsv.value_range(ask_period)
    assert min_ts == pytest.approx(2.)
    assert max_ts == pytest.approx(6.)


