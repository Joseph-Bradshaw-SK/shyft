#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

        inline request_reply mk_request_reply(string const&rid,string const& ex_info) {
            return request_reply{rid,ex_info};
        }
        template<typename Iterator,typename Skipper>
        request_reply_grammar<Iterator,Skipper>::request_reply_grammar():request_reply_grammar::base_type(start,"request_reply_grammar") {
           start = (
            lit('{')
            >> lit("\"request_id\"")       >>lit(':')  >>quoted_string_   >> lit(',')
            >> lit("\"diagnostics\"")      >>lit(':')  >>quoted_string_
            >> lit('}')
            )[_val=phx::bind(mk_request_reply, _1,_2)]
            ;
            on_error<fail>(start, error_handler(_4, _3, _2));
        }
        template struct request_reply_grammar<request_iterator_t,request_skipper_t>;


}
