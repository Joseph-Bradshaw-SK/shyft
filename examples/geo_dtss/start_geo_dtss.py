from shyft.time_series import DtsServer,DtssCfg
from time import sleep

def start_geo_dtss(root_dir: str, port_no: int , compress:bool, cache_target_mb:int )->DtsServer:
    s = DtsServer()
    s.cache_ts_initial_size_estimate=8*24*10 # 2k, get initial guess there ar many small ts.
    s.cache_max_items=20*1000*1000 # 20 mill timeseries
    s.cache_memory_target=cache_target_mb*1000*1000  # 5000 MB is ok.
    M = 1024*1024
    fc_cfg = DtssCfg(ppf=10*M, compress=compress, max_file_size=100*M, write_buffer_size=4*M,log_level=200)
    s.default_geo_db_config=fc_cfg  # if the client creates new geo db, these settings will apply.
    s.set_container('', root_dir, 'ts_ldb', fc_cfg)
    # In order to get the geo_dbs to use the above set DtssCfg, containers can be set in advance on server side
    # or, we can utilize the default cfg, also set.
    # commented out to illustrate the point here: s.set_container('fc', root_dir+"/fc", 'ts_ldb', fc_cfg)
    s.set_container('cc', root_dir+"/cc", 'ts_ldb', fc_cfg)
    s.set_listening_port(port_no)
    s.start_async()

    return s

# Create and start a server instance `s`. We must keep a reference to `s`, otherwise it is garbage collected
# Using port 20000 as example, but any available port should work.
# If you shall access the server from other hosts, then remember to open firewall for the selected port
s=start_geo_dtss('./dtss_root', port_no=20000,compress=False, cache_target_mb=5000)
while True:
    sleep(2)
    c = s.cache_stats
    print(f'cache # = {c.id_count} #hits={c.hits} #misses={c.misses} #point-sz={c.point_count*8/1e6} Mbytes')
