#include <doctest/doctest.h>

#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/price_delivery_convert.h>

using namespace shyft::energy_market::stm;
using std::make_shared;
using std::vector;
using std::map;
using std::runtime_error;
using xy_point=shyft::energy_market::hydro_power::point;
using xy_points=shyft::energy_market::hydro_power::xy_point_curve;
using shyft::energy_market::hydro_power::xy_point_curve_;
using shyft::energy_market::stm::convert_to_price_delivery_tsv;

TEST_SUITE("stm_energy_market_area") {
    TEST_CASE("energy_market_area_construct") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto ema=make_shared<energy_market_area>(1,"ema","{}", sys);
        FAST_CHECK_UNARY( ema);
        FAST_CHECK_EQ(ema->id,1);
        FAST_CHECK_EQ(ema->name,"ema");
        FAST_CHECK_EQ(ema->json,"{}");
    }
    TEST_CASE("energy_market_area_set_unit_group") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto ema=make_shared<energy_market_area>(1,"ema","{}", sys);
        auto ug1 = sys->add_unit_group(1,"ug1","{}",unit_group_type::production);
        ema->set_unit_group(ug1);
        FAST_CHECK_EQ(ema->unit_groups[0], ug1);
        auto ug2 = sys->add_unit_group(2,"ug2","{}");
        CHECK_THROWS_AS(ema->set_unit_group(ug2), std::runtime_error);
    }
    TEST_CASE("energy_market_area_get_unit_group") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto ema=make_shared<energy_market_area>(1,"ema","{}", sys);
        auto ug1 = sys->add_unit_group(1,"ug1","{}",unit_group_type::production);
        auto ug = ema->get_unit_group();
        FAST_CHECK_EQ(ug, nullptr);
        ema->set_unit_group(ug1);
        auto ug2 = ema->get_unit_group();
        FAST_CHECK_EQ(ug2, ug1);
        FAST_CHECK_EQ(ema->unit_groups.size(),1u);
    }
    TEST_CASE("energy_market_area_remove_group") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto ema=make_shared<energy_market_area>(1,"ema","{}", sys);
        auto ug1 = sys->add_unit_group(1,"ug1","{}",unit_group_type::production);
        CHECK_THROWS_AS(ema->remove_unit_group(), std::runtime_error);
        ema->set_unit_group(ug1);
        auto ug = ema->remove_unit_group();
        FAST_CHECK_EQ(ug, ug1);
        FAST_CHECK_EQ(ema->unit_groups.size(),0u);
    }
    TEST_CASE("energy_market_area_generate_url") {
        auto sys=make_shared<stm_system>(123,"sys","{}");
        auto ema=make_shared<energy_market_area>(456,"ema","{}", sys);
        std::string s;
        auto rbi=std::back_inserter(s);
        ema->generate_url(rbi);
        FAST_CHECK_EQ(s,"/m456");
        s.clear();
        ema->generate_url(rbi, -1, -1);
        FAST_CHECK_EQ(s,"/m456");
        s.clear();
        ema->generate_url(rbi, -1, 0);
        FAST_CHECK_EQ(s,"/m{o_id}");
        s.clear();
        ema->generate_url(rbi, -1, 1);
        FAST_CHECK_EQ(s,"/m456");
        s.clear();
    }
}
