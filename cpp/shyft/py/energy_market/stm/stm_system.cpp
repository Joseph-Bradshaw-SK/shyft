/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_tsm_expose.h>
#include <shyft/energy_market/stm/srv/dstm/ts_url_generator.h>
#include <boost/format.hpp>
#include <stdexcept>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/unit_group.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;
    using namespace std::string_literals;

    using shyft::time_series::dd::apoint_ts;

    using std::string;
    using std::string_view;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;
    using std::runtime_error;

    using boost::format;

    template<> string str_(stm::energy_market_area::ts_triplet_ const& o) {
        return (format("_TsTriplet(realised=%1%,schedule=%2%,result=%3%)")
            %str_(o.realised)
            %str_(o.schedule)
            %str_(o.result)
        ).str();
    }

    template<> string str_(stm::energy_market_area::offering_ const& o) {
        return (format("_Offering(bids=%1%,usage=%2%,price=%3%)")
            %str_(o.bids)
            %str_(o.usage)
            %str_(o.price)
        ).str();
    }

    template<> string str_(stm::energy_market_area const& o) {
        return (format("MarketArea(id=%1%, name=%2%)")
            %str_(o.id)
            %str_(o.name)
        ).str();
    }


    template<> string str_(stm::stm_system const& o) {
        return (format("StmSystem(id=%1%, name=%2%)")
            %str_(o.id)
            %str_(o.name)
        ).str();
    }

    struct stm_sys_ext {
        static std::vector<char> to_blob(const  stm::stm_system_& m) {
            auto s=shyft::energy_market::stm::stm_system::to_blob(m);
            return std::vector<char>(s.begin(),s.end());
        }
        static stm::stm_system_ from_blob(std::vector<char>& blob) {
            std::string s(blob.begin(),blob.end());
            return shyft::energy_market::stm::stm_system::from_blob(s);
        }
    };


    void stm_system() {
        auto ma=py::class_<
            stm::energy_market_area,
            py::bases<>,
            shared_ptr<stm::energy_market_area>,
            boost::noncopyable
        >("MarketArea", 
            doc_intro("A market area, with load/price and other properties.")
            doc_details("Within the market area, there are zero or more energy-\n"
                        "producing/consuming units.")
            , py::no_init);
        ma
            .def(py::init<int, const string&, const string&, stm::stm_system_ &>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("stm_sys")),
                "Create market area for a stm system."))
            .def_readwrite("id",&stm::energy_market_area::id,"Unique id for this object.")
            .def_readwrite("name",&stm::energy_market_area::name,"Name for this object.")
            .def_readwrite("json",&stm::energy_market_area::json,"Json keeping any extra data for this object.")
            .add_property("tag",+[](const stm::energy_market_area& self){return url_tag(self);}, "Url tag.")
            .add_property("unit_group",&stm::energy_market_area::get_unit_group,&stm::energy_market_area::set_unit_group, "Getter and setter for unit group.")
            .def_readonly("contracts",&stm::energy_market_area::contracts,"List of contracts.")
            .def_readonly("demand",&stm::energy_market_area::demand,"The demand side describing the buyers of energy")
            .def_readonly("supply",&stm::energy_market_area::supply,"The supply side describing the producers of energy")
            .def("remove_unit_group",&stm::energy_market_area::remove_unit_group,(py::arg("self")),
                doc_intro("Remove the unit group associated with this market if available.")
                doc_parameters()
                doc_returns("unit_group","UnitGroup","The newly removed unit group.")
            )
            .def("flattened_attributes", +[](stm::energy_market_area& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
            .def(py::self==py::self)
            .def(py::self!=py::self)
        ;
        expose_tsm(ma);
        expose_str_repr(ma);
        add_proxy_property(ma,"load", stm::energy_market_area,load, "[W] Load, time series.")
        add_proxy_property(ma,"price", stm::energy_market_area,price, "[Money/J] Price, time series.")
        add_proxy_property(ma,"max_buy", stm::energy_market_area,max_buy, "[W] Maximum buy, time series.")
        add_proxy_property(ma,"max_sale", stm::energy_market_area,max_sale, "[W] Maximum sale, time series.")
        add_proxy_property(ma,"buy", stm::energy_market_area,buy, "[W] Buy result, time series.")
        add_proxy_property(ma,"sale", stm::energy_market_area,sale, "[W] Sale result, time series.")
        add_proxy_property(ma,"production", stm::energy_market_area,production, "[W] Production result, time series.")
        add_proxy_property(ma,"reserve_obligation_penalty", stm::energy_market_area,reserve_obligation_penalty, "[Money/x] Obligation penalty, time series.")
        /* ns scope */{
            py::scope offering{ma};
            auto offr=py::class_<stm::energy_market_area::offering_,py::bases<>,boost::noncopyable>(
                "_Offering",
                doc_intro("describes actors (supply/demand) offering into the market"),
                py::no_init
            );
            offr
            .def_readonly("usage",&stm::energy_market_area::offering_::usage,"The amount consumed/used of the offering")
            .def_readonly("price",&stm::energy_market_area::offering_::price,"Given the usage, the effective price based on bids")
            ;
            expose_str_repr(offr);
            _add_proxy_property(offr,"bids",stm::energy_market_area::offering_,bids,"time dep x,y, x=price[Money/J],y=amount[W]")
            auto ts3=py::class_<stm::energy_market_area::ts_triplet_,py::bases<>,boost::noncopyable>(
                "_TsTriplet",
                doc_intro("describes .realised, .schedule and .result time-series"),
                py::no_init
            );
            _add_proxy_property(ts3,"realised",stm::energy_market_area::ts_triplet_,realised,"SI-units, realised time-series, as in historical fact")
            _add_proxy_property(ts3,"schedule",stm::energy_market_area::ts_triplet_,schedule,"SI-units, schedule, as in current schedule")
            _add_proxy_property(ts3,"result",stm::energy_market_area::ts_triplet_,result,"SI-units, result, as provided by optimisation")
            expose_str_repr(ts3);
        }

        expose_vector_eq<stm::energy_market_area_>("MarketAreaList","A strongly typed list of MarketArea.",&stm::equal_attribute<std::vector<stm::energy_market_area_>>,false);
        auto ss= py::class_<
            stm::stm_system,
            py::bases<>,
            shared_ptr<stm::stm_system>,
            boost::noncopyable
        >("StmSystem", "A complete stm system, with market areas, and hydro power systems.", py::no_init);
        ss
            .def(py::init<int, const string&, const string&>(
                (py::arg("uid"), py::arg("name"), py::arg("json")), "Create stm system."))
            .def_readwrite("id",&stm::stm_system::id,"Unique id for this object.")
            .def_readwrite("name",&stm::stm_system::name,"Name for this object.")
            .def_readwrite("json",&stm::stm_system::json,"Json keeping any extra data for this object.")
            .def_readonly("market_areas",&stm::stm_system::market,"List of market areas.")
            .def_readonly("contracts",&stm::stm_system::contracts,"List of contracts.")
            .def_readonly("contract_portfolios",&stm::stm_system::contract_portfolios,"List of contract portfolios.")
            .def_readonly("hydro_power_systems",&stm::stm_system::hps,"List of hydro power systems.")
            .def_readonly("power_modules", &stm::stm_system::power_modules, "List of power modules.")
            .def_readonly("networks", &stm::stm_system::networks, "List of networks.")
            .def_readonly("summary", &stm::stm_system::summary, "Key result values from simulations.")
            .def_readonly("run_parameters", &stm::stm_system::run_params, "Run parameters.")
            .def_readonly("unit_groups",&stm::stm_system::unit_groups,
                doc_intro("Unit groups with constraints.")
                doc_intro("These groups holds a list of units, plus the constraints that apply to them.")
                doc_intro("During optimisation, the groups along with their constraints,")
                doc_intro("so that the optimization can take it into account when finding the")
                doc_intro("optimal solution.")
            )
            .def("add_unit_group",&stm::stm_system::add_unit_group,(py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json"), py::arg("group_type")=0),
                doc_intro("Add an empty named unit-group to the system.")
                doc_parameters()
                doc_parameter("id","int","Unique id of the group.")
                doc_parameter("name","str","Unique name of the group.")
                doc_parameter("json","str","Json payload for py customization.")
                doc_parameter("group_type","int", "One of UnitGroup.unit_group_type values, default 0.")
                doc_returns("unit_group","UnitGroup","The newly created unit-group.")
            )
            .def("result_ts_urls",+[](stm::stm_system const*s,string prefix){
                    return shyft::energy_market::stm::srv::dstm::ts_url_generator(prefix,*s);
                },
                 (py::arg("self"),py::arg("prefix")),
                 doc_intro("Generate and return all result time series urls for the specified model.")
                 doc_intro("Useful in the context of a dstm client, that have a get_ts method that accepts this list.")
                 doc_parameters()
                 doc_parameter("prefix","","Url prefix, like dstm://Mmodel_id.")
                 doc_returns("ts_urls","StringVector","A strongly typed list of strings with ready to use ts-urls.")
             )
            .def("to_blob",&stm_sys_ext::to_blob,(py::arg("self")),
                doc_intro("Serialize the model to a blob.")
                doc_returns("blob","ByteVector","Blob form of the model.")
            )
            .def("from_blob",&stm_sys_ext::from_blob,(py::arg("blob")),
                doc_intro("Re-create a stm system from a previously create blob.")
                doc_returns("stm_sys","StmSystem","A stm system including hydro-power-systems and markets.")
            ).staticmethod("from_blob")
            .def(py::self==py::self)
            .def(py::self!=py::self)
        ;
        expose_str_repr(ss);

        expose_vector_eq<shared_ptr<stm::stm_system>>("StmSystemList","Strongly typed list of StmSystem",&stm::equal_attribute<std::vector<shared_ptr<stm::stm_system>>>,false);

   }
}
