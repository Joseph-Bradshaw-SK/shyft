#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/em_utils.h>

namespace shyft::energy_market::stm {
    namespace hana = boost::hana;
    namespace mp = shyft::mp;

    bool network::operator==(network const&o) const {
        if(this==&o) return true;//equal by addr.
        return super::operator==(o)
              && equal_vector_ptr_content<transmission_line>(transmission_lines,o.transmission_lines)
              && equal_vector_ptr_content<busbar>(busbars,o.busbars);
    }

    void network::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
        if (levels) {
            auto tmp = dynamic_pointer_cast<stm_system>(sys_());
            if (tmp) tmp->generate_url(rbi, levels-1, template_levels ? template_levels - 1 : template_levels);
        }
        if (!template_levels) {
            constexpr std::string_view a = "/n{o_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto idstr="/n"+std::to_string(id);
            std::copy(std::begin(idstr),std::end(idstr),rbi);
        }
    }
}
