#include <doctest/doctest.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/busbar.h>

using namespace shyft::energy_market::stm;
using std::make_shared;

namespace {
    struct test_sys {
        stm_system_ sys;
        network_ net;
        power_module_ pm;
        busbar_ b;
        test_sys() {
            sys=make_shared<stm_system>(1,"sys","{}");
            net=make_shared<network>(2,"net", "{}", sys);
            sys->networks.push_back(net);
            pm=make_shared<power_module>(3,"pm","{}", sys);
            sys->power_modules.push_back(pm);
            b=make_shared<busbar>(4,"b","{}", net);
            net->busbars.push_back(b);
            b->add_to_power_module(pm);
        }
    };
}

TEST_SUITE("stm_power_module") {

    TEST_CASE("power_module_construct") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto pm=make_shared<power_module>(1,"pm","{}", sys);
        FAST_CHECK_UNARY(pm);
        FAST_CHECK_EQ(pm->id,1);
        FAST_CHECK_EQ(pm->name,"pm");
        FAST_CHECK_EQ(pm->json,"{}");
    }

    TEST_CASE("power_module_equality") {
        test_sys sys1;
        test_sys sys2;
        CHECK_EQ(*sys1.pm, *sys1.pm); // equal to self
        CHECK_EQ(*sys1.pm, *sys2.pm); 
    }
}
