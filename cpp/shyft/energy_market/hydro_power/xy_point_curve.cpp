/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <algorithm>
#include <stdexcept>
#include <boost/format.hpp>

#include <shyft/energy_market/hydro_power/xy_point_curve.h>



namespace shyft::energy_market::hydro_power {

    using std::sort;
    using std::runtime_error;
    using std::transform;
    using std::min_element;
    using std::max_element;

    xy_point_curve::xy_point_curve(vector <point> const &points_a) : points(points_a) {
        sort(points.begin(), points.end());
    }

    xy_point_curve::xy_point_curve(const vector<double> &x, const vector<double> &y) {
        if (x.size() != y.size())
            throw runtime_error((boost::format("x points size %1% and y points size %2% lists must have same dimension") % x.size() % y.size()).str());
        points.reserve(x.size());
        for (size_t i = 0; i < x.size(); ++i)
            points.emplace_back(x[i], y[i]);
        sort(points.begin(), points.end());
    }

    bool xy_point_curve::is_xy_mono_increasing() const {
        if (points.size() < 2)
            return false;
        for (size_t i = 1; i < points.size(); i++) {
            if (points[i - 1].x >= points[i].x || points[i - 1].y >= points[i].y)
                return false;
        }
        return true;
    }

    bool xy_point_curve::is_xy_invertible() const {
        if (points.size() < 2)
            return false;

	const int sgn = (points.back().y > points.front().y) - (points.back().y < points.front().y);
	auto non_monotone_segment = [&sgn] (const auto& a, const auto&b) {return (b.x <= a.x || b.y * sgn <= a.y * sgn);};

	return  std::adjacent_find(points.begin(), points.end(), non_monotone_segment) == points.end();
    }

    bool xy_point_curve::is_xy_convex() const {
        if (points.size() < 2)
            return false;

        // Check for non-increasing x-values and reentrant corners
        double dydx = -std::numeric_limits<double>::infinity();
        auto non_convex_segment = [&dydx] (const auto& a, const auto& b) -> bool {
            auto next_dydx = (b.y - a.y) / (b.x - a.x);
            if (b.x <= a.x || next_dydx < dydx)
                return true;
            dydx = next_dydx;
            return false;
        };
        return std::adjacent_find(points.begin(), points.end(), non_convex_segment) == points.end();
    }
    double xy_point_curve::calculate_y(double x) const {
        if (points.size() == 0)
            return std::numeric_limits<double>::quiet_NaN();
        if (points.size() == 1)
            return points.back().y;
        apoint_ts x_ts(time_axis::fixed_dt(0.0, 1.0, 1), x, shyft::time_series::POINT_AVERAGE_VALUE);
        return calculate_y(x_ts).values().front();
    }

    apoint_ts xy_point_curve::calculate_y(const apoint_ts& x, interpolation_scheme scheme) const {
        vector<std::array<double, 2>> pts;
        for (const auto& p: points)
            pts.push_back(std::array<double, 2>{p.x, p.y});

        return x.transform(pts, scheme);
    }

    double xy_point_curve::calculate_x(double from_y) const {
        if (points.size() < 2)
            return std::numeric_limits<double>::quiet_NaN();
        apoint_ts y_ts(time_axis::fixed_dt(0.0, 1.0, 1), from_y, shyft::time_series::POINT_AVERAGE_VALUE);
        return calculate_x(y_ts).values().front();
    }

    apoint_ts xy_point_curve::calculate_x(const apoint_ts& y, interpolation_scheme scheme) const {
        if (!is_xy_invertible())
            throw runtime_error("xy_point_curve is not invertible.");
	        
        vector<std::array<double, 2>> pts;
        for (const auto& p: points)
            pts.push_back(std::array<double, 2>{p.y, p.x});

        // Sorting is needed for the case that y-values are decreasing
        std::sort(pts.begin(), pts.end());

        return y.transform(pts, scheme);
    }

    // Functions for determining min and max of curves are implement as free functions. The reason for this is that
    // the data structure for the R^2 -> R mapping is a std::vector and this bars implementation as class functions.

    auto compare_x = [] (const auto& a, const auto& b) -> bool {return a.x < b.x; };
    auto compare_y = [] (const auto& a, const auto& b) -> bool {return a.y < b.y; };
    auto compare_z = [] (const auto& a, const auto& b) -> bool {return a.z < b.z; };

    auto NaN = std::numeric_limits<double>::quiet_NaN();

    // Return nan for empty tables. Using std::optional or throwing error could be better choices here.
    double x_min(const xy_point_curve& xy) { return xy.points.size() ? min_element(xy.points.begin(), xy.points.end(), compare_x)->x : NaN; }
    double x_max(const xy_point_curve& xy) { return xy.points.size() ? max_element(xy.points.begin(), xy.points.end(), compare_x)->x : NaN; }
    double y_min(const xy_point_curve& xy) { return xy.points.size() ? min_element(xy.points.begin(), xy.points.end(), compare_y)->y : NaN; }
    double y_max(const xy_point_curve& xy) { return xy.points.size() ? max_element(xy.points.begin(), xy.points.end(), compare_y)->y : NaN; }

    double x_min(const xy_point_curve_with_z& xyz) { return x_min(xyz.xy_curve); };
    double x_max(const xy_point_curve_with_z& xyz) { return x_max(xyz.xy_curve); };
    double y_min(const xy_point_curve_with_z& xyz) { return y_min(xyz.xy_curve); };
    double y_max(const xy_point_curve_with_z& xyz) { return y_max(xyz.xy_curve); };

    double x_min(const vector<xy_point_curve_with_z>& xyz) {
        vector<double> x;
        transform(xyz.begin(), xyz.end(), back_inserter(x), [] (const auto& xy) -> double { return x_min(xy); });
        return x.size() ? *min_element(x.begin(), x.end()) : NaN;
    }

    double x_max(const vector<xy_point_curve_with_z>& xyz) {
        vector<double> x;
        transform(xyz.begin(), xyz.end(), back_inserter(x), [] (const auto& xy) -> double { return x_max(xy); });
        return x.size() ? *max_element(x.begin(), x.end()) : NaN;
    }

    double y_min(const vector<xy_point_curve_with_z>& xyz) {
        vector<double> y;
        transform(xyz.begin(), xyz.end(), back_inserter(y), [] (const auto& xy) -> double { return y_min(xy); });
        return y.size() ? *min_element(y.begin(), y.end()) : NaN;
    }

    double y_max(const vector<xy_point_curve_with_z>& xyz) {
        vector<double> y;
        transform(xyz.begin(), xyz.end(), back_inserter(y), [] (const auto& xy) -> double { return y_max(xy); });
        return y.size() ? *max_element(y.begin(), y.end()) : NaN;
    }

    double z_min(const vector<xy_point_curve_with_z>& xyz) {
        return xyz.size() ? min_element(xyz.begin(), xyz.end(), compare_z)->z : NaN;
    }

    double z_max(const vector<xy_point_curve_with_z>& xyz) {
        return xyz.size() ? max_element(xyz.begin(), xyz.end(), compare_z)->z : NaN;
    }

}
