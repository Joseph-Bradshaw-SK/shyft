import pytest

from shyft.dashboard.time_series.attr_callback_manager import AttributeCallbackManager, _check_callback
from shyft.dashboard.base.hashable import Hashable


def test_attribute_callback_manager():

    class Foo(AttributeCallbackManager):

        def __init__(self):
            super().__init__()
            self.a = 10

    class Bar:
        call_count = 0

        def changed_a(self, obj, attr, old_value, new_value) -> None:
            self.call_count += 1
            assert isinstance(obj, Foo)
            assert attr == 'a'
            assert old_value == 10
            assert new_value == 20

    b = Bar()

    f = Foo()
    # Test add callback
    f.on_change(obj=b, attr='a', callback=b.changed_a)
    # test change variable
    f.a = 20
    assert b.call_count == 1
    # check that callbacks are not run if value is set twice
    f.a = 20
    assert b.call_count == 1

    # try to add value once more
    assert not f.on_change(obj=b, attr='a', callback=b.changed_a)

    # test remove callback
    f.remove_all_callbacks(obj=b)
    # test
    f.a = 10


    # check if ValueError is raised if we try to add something which we do not have
    with pytest.raises(ValueError):
        f.on_change(obj=b, attr='c', callback=b.changed_a)


def test_attribute_callback_manager_with_hashable():

    class Foo(AttributeCallbackManager, Hashable):

        def __init__(self):
            AttributeCallbackManager.__init__(self)
            Hashable.__init__(self)

            self.a = 10

    class Bar:
        def changed_a(self, obj, attr, old_value, new_value) -> None:
            assert isinstance(obj, Foo)
            assert attr == 'a'
            assert old_value == 10
            assert new_value == 20

    b = Bar()

    f = Foo()
    # Check if uid is working/ hashable
    f.uid
    # Test add callback
    f.on_change(obj=b, attr='a', callback=b.changed_a)
    # test change variable
    f.a = 20
    # test remove callback
    f.remove_all_callbacks(obj=b)
    # test
    f.a = 10


def test_check_callback():

    def foo(obj, value):
        pass

    # assert good check
    assert _check_callback(foo, ('obj', 'value'))

    # assert wrong variable name
    with pytest.raises(RuntimeError):
        _check_callback(foo, ('obj', 'new_value'))

    # assert not enough variables
    with pytest.raises(RuntimeError):
        _check_callback(foo, ('obj'))

    # assert too much variables
    with pytest.raises(RuntimeError):
        _check_callback(foo, ('obj', 'bar', 'value'))

    # assert wrong
    with pytest.raises(RuntimeError):
        _check_callback(foo, ('value', 'obj'))

