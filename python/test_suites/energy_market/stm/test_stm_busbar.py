import pytest
from functools import reduce
from shyft.energy_market.stm import StmSystem, Busbar, Network, TransmissionLine, PowerModule

def test_busbar_attributes():
    sys = StmSystem(1, "A", "{}")
    n = Network(2, 'Network', '{}', sys)
    sys.networks.append(n)
    b = Busbar(3, 'Busbar #1', '{}', n)
    n.busbars.append(b)
    # generics, from id_base
    assert hasattr(b, "id")
    assert hasattr(b, "name")
    assert hasattr(b, "json")
    # specifics
    assert hasattr(b, "dummy")
    assert b.tag == "/n2/b3"

def test_busbar_attributes_flattened():
    sys = StmSystem(1, "A", "{}")
    n = Network(2, 'Network', '{}', sys)
    sys.networks.append(n)
    b = Busbar(3, 'Busbar #1', '{}', n)
    n.busbars.append(b)
    assert b.flattened_attributes()
    for (path, val) in b.flattened_attributes().items():
        attr = reduce(getattr, path.split('.'), b)
        assert type(val) is type(attr)
        assert val == attr
        assert val.url() == attr.url()

def test_transmission_line_association():
    sys = StmSystem(1, "A", "{}")
    n = Network(2, 'Network', '{}', sys)
    sys.networks.append(n)
    t = TransmissionLine(3, 'TransmissionLine #1', '{}', n)
    n.transmission_lines.append(t)
    b1 = Busbar(3, 'Busbar #1', '{}', n)
    b2 = Busbar(4, 'Busbar #2', '{}', n)
    n.busbars.extend([b1, b2])
    # Associate busbar to start of transmission line
    b1.add_to_start_of_transmission_line(t)
    assert b1.get_transmission_lines_from_busbar()[0] == t
    assert len(b1.get_transmission_lines_to_busbar()) == 0
    assert t.from_bb.id == 3
    # Associate busbar to end of transmission line
    b2.add_to_end_of_transmission_line(t)
    assert len(b2.get_transmission_lines_from_busbar()) == 0
    assert b2.get_transmission_lines_to_busbar()[0] == t
    assert t.to_bb.id == 4

def test_power_module_association():
    sys = StmSystem(1, "A", "{}")
    pm = PowerModule(2, 'PowerModule #1', '{}', sys)
    sys.power_modules.append(pm)
    n = Network(3, 'Network', '{}', sys)
    sys.networks.append(n)
    b = Busbar(4, 'Busbar #1', '{}', n)
    n.busbars.append(b)
    b.add_to_power_module(pm);
    pms = b.get_power_modules();
    assert len(pms) == 1
    assert pms[0].id == 2

def test_busbar_equal():
    sys = StmSystem(1, "A", "{}")
    n = Network(2, 'Network', '{}', sys)
    sys.networks.append(n)
    a = Busbar(3, 'Busbar #1', '{}', n)
    b = Busbar(3, 'Busbar #1', '{}', n)
    c = Busbar(4, 'hmm', '{}', n)
    n.busbars.extend([a, b, c])
    assert len(n.busbars) == 3
    assert a == a
    assert a == b
    assert a != c

def test_busbar_equal_with_associations():
    sys = StmSystem(1, "A", "{}")
    pm = PowerModule(2, 'PowerModule #1', '{}', sys)
    pm_2 = PowerModule(2, 'PowerModule #1', '{}', sys)
    sys.power_modules.extend([pm, pm_2])
    n = Network(2, 'Network', '{}', sys)
    sys.networks.append(n)
    t = TransmissionLine(3, 'TransmissionLine #1', '{}', n)
    t_2 = TransmissionLine(3, 'TransmissionLine #1', '{}', n)
    t2 = TransmissionLine(4, 'TransmissionLine #2', '{}', n)
    t2_2 = TransmissionLine(4, 'TransmissionLine #2', '{}', n)
    n.transmission_lines.extend([t, t_2, t2, t2_2])
    b = Busbar(3, 'Busbar #1', '{}', n)
    b_2 = Busbar(3, 'Busbar #1', '{}', n)
    n.busbars.extend([b, b_2])
    assert b == b_2
    # First
    b.add_to_power_module(pm)
    b.add_to_start_of_transmission_line(t)
    b.add_to_end_of_transmission_line(t2)
    assert b != b_2
    # Second
    b_2.add_to_power_module(pm_2)
    b_2.add_to_start_of_transmission_line(t_2)
    b_2.add_to_end_of_transmission_line(t2_2)
    assert b == b_2