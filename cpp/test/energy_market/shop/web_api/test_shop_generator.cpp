#include <doctest/doctest.h>

#include <shyft/time/utctime_utilities.h>
#include <shyft/web_api/energy_market/stm/shop/generators.h>
#include <shyft/energy_market/stm/shop/shop_log_entry.h>

using shyft::core::from_seconds;
using shyft::energy_market::stm::shop::shop_log_entry;
using std::string;
using boost::spirit::karma::generate;

TEST_SUITE("shop_generators") {
    using shyft::web_api::generator::shop::shop_log_entry_generator;
    TEST_CASE("shop_log_entry") {
        // Create a message
        shop_log_entry msg{shop_log_entry::log_severity::diagnosis_warning, "test message", 123, from_seconds(10)};
        // Create generator:
        string ps;
        auto sink = std::back_inserter(ps);
        shop_log_entry_generator<decltype(sink)> g;

        // Generate string:
        REQUIRE(generate(sink, g, msg));
        // Verify string:
        CHECK_EQ(ps, R"_({"time":10.0,"severity":"DIAGNOSIS WARNING","code":123,"message":"test message"})_");
    }
}
