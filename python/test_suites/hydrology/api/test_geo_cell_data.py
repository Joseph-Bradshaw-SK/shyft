from shyft import hydrology as api
import pytest

"""Verify and illustrate GeoCellData exposure to python
   
 """


def test_create():
    p = api.GeoPoint(100, 200, 300)
    ltf = api.LandTypeFractions()
    ltf.set_fractions(glacier=0.1, lake=0.1, reservoir=0.1, forest=0.1)
    assert round(abs(ltf.unspecified() - 0.6), 7) == 0
    routing_info = api.RoutingInfo(2, 12000.0)
    gcd = api.GeoCellData(p, 1000000.0, 1, 0.9, ltf, routing_info)

    assert round(abs(gcd.area() - 1000000), 7) == 0
    assert round(abs(gcd.catchment_id() - 1), 7) == 0
    assert round(abs(gcd.routing_info.distance - 12000.0), 7) == 0
    assert round(abs(gcd.routing_info.id - 2), 7) == 0
    gcd.routing_info.distance = 13000.0
    gcd.routing_info.id = 3
    assert round(abs(gcd.routing_info.distance - 13000.0), 7) == 0
    assert round(abs(gcd.routing_info.id - 3), 7) == 0
    gcd.set_catchment_id(10)  # verify it works
    assert gcd.catchment_id() == 10


def test_tin_data():
    p = api.GeoPoint(1000, 1000, 100)
    ltf = api.LandTypeFractions()
    ltf.set_fractions(glacier=1.0, lake=0.0, reservoir=0.0, forest=0.0)
    assert ltf.unspecified() == pytest.approx(0.0)
    routing_info = api.RoutingInfo(2, 12000.0)
    p1 = api.GeoPoint(0, 500, 100)
    p2 = api.GeoPoint(2000, 500, 100)
    p3 = api.GeoPoint(1000, 2000, 100)
    gcd = api.GeoCellData(p1, p2, p3, epsg_id=32632, catchment_id=1, land_type_fractions=ltf, routing_info=routing_info)

    assert gcd.area() == pytest.approx(1500000)
    assert gcd.slope() == pytest.approx(0)
    # assert gcd.aspect(), 0)
    assert gcd.vertexes()[0].x == pytest.approx(p1.x)
    assert gcd.vertexes()[0].y == pytest.approx(p1.y)
    assert gcd.vertexes()[0].z == pytest.approx(p1.z)
    assert gcd.vertexes()[1].x == pytest.approx(p2.x)
    assert gcd.vertexes()[1].y == pytest.approx(p2.y)
    assert gcd.vertexes()[1].z == pytest.approx(p2.z)
    assert gcd.vertexes()[2].x == pytest.approx(p3.x)
    assert gcd.vertexes()[2].y == pytest.approx(p3.y)
    assert gcd.vertexes()[2].z == pytest.approx(p3.z)
    assert gcd.mid_point().x == pytest.approx(p.x)
    assert gcd.mid_point().y == pytest.approx(p.y)
    assert gcd.mid_point().z == pytest.approx(p.z)
    assert gcd.catchment_id() == 1
    assert gcd.routing_info.distance == pytest.approx(12000.0)
    assert gcd.routing_info.id == 2
    gcd.routing_info.distance = 13000.0
    gcd.routing_info.id = 3
    assert gcd.routing_info.distance == pytest.approx(13000.0)
    assert gcd.routing_info.id == 3
    gcd.set_catchment_id(10)  # verify it works
    assert gcd.catchment_id() == 10


def test_land_type_fractions():
    """ 
     LandTypeFractions describes how large parts of a cell is 
     forest,glacier, lake ,reservoir, - the rest is unspecified
     The current cell algorithms like ptgsk uses this information
     to manipulate the response.
     e.g. precipitation that falls into the reservoir fraction goes directly to 
     the response (the difference of lake and reservoir is that reservoir is a lake where
     we store water to the power-plants.)
     
    """
    # constructor 1 :all in one: specify glacier_size,lake_size,reservoir_size,forest_size,unspecified_size
    a = api.LandTypeFractions(1000.0, 2000.0, 3000.0, 4000.0, 5000.0)  # keyword arguments does not work ??
    assert a == a
    # constructor 2: create, and set (with possible exceptions)
    b = api.LandTypeFractions()
    assert a != b
    b.set_fractions(glacier=1/15.0, lake=2/15.0, reservoir=3/15.0, forest=4/15.0)
    assert round(abs(a.glacier() - b.glacier()), 7) == 0
    assert round(abs(a.lake() - b.lake()), 7) == 0
    assert round(abs(a.reservoir() - b.reservoir()), 7) == 0
    assert round(abs(a.forest() - b.forest()), 7) == 0
    assert round(abs(a.unspecified() - b.unspecified()), 7) == 0
    assert round(abs(a.snow_storage() - (1.0 - a.lake() - a.reservoir())), 7) == 0
    try:

        b.set_fractions(glacier=0.9, forest=0.2, lake=0.0, reservoir=0.0)
        assert False, "expected exception, nothing raised"
    except:
        assert True, "If we reach here all is ok"
