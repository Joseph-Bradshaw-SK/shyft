/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>

namespace shyft::energy_market::hydro_power {

    using std::dynamic_pointer_cast;
	using std::runtime_error;
            

    reservoir_ const& reservoir::input_from(reservoir_ const& me, waterway_ const& w) {
        connect(w->shared_from_this(), me->shared_from_this());
        return me; 
    }
    reservoir_ const& reservoir::output_to(reservoir_ const& me, waterway_ const& w, connection_role role) {
        connect(me->shared_from_this(), role, w->shared_from_this());
        return me;
    }
    reservoir_ reservoir::shared_from_this() const {
        auto hps=hps_();
        return hps?shared_from_me(this,hps->reservoirs):nullptr;
    }

    bool reservoir::operator==(const reservoir&o)const {
        return this==&o ||(id_base::operator==(o) && equal_structure(o));// same attribs, and closure/neighbours
    }

}
