#
# Testing Python stringification of STM specific XY types.
# Exposed from shyft/cpp/shyft/py/energy_market/stm/expose_str.cpp
# Format is JSON-like, but not valid json - e.g. property names are not always quoted.
#
# Note that base energy_market XY types have separate stringification!
# Exposed from shyft/cpp/shyft/py/energy_market/core/api.cpp
# E.g. str() of a t_xy will use the STM stringification, but str() of t_txy[t0]
# returns a shyft.energy_market.core._core.XyPointCurve and will use base
# stringification!
# The energy market version uses different syntax not JSON-like but more
# verbose lists, example for point list:
# - STM:
#     [(20.000000,96.000000),(40.000000,98.000000),(60.000000,99.000000)]
# - Energy Market:
#     PointList([
#       Point(20.000000, 96.000000),
#       Point(40.000000, 98.000000),
#       Point(60.000000, 99.000000),
#       Point(60.000000, 99.000000),
#       Point(60.000000, 99.000000)])
#
from shyft.energy_market.core import Point, PointList, XyPointCurve, XyPointCurveList, XyPointCurveWithZ, XyPointCurveWithZList, TurbineEfficiency, TurbineEfficiencyList, TurbineDescription
from shyft.energy_market.stm import t_xy, t_xyz, t_turbine_description
from shyft.time_series import time
t0 = time('2000-01-01T00:00:00Z')
t1 = time('2022-01-01T00:00:00Z')

def test_t_xy():

    # Test case:
    t = t_xy()
    t[t0] = XyPointCurve(PointList([]))
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: []\n}'
    # Expected result:
    #{
    #        2000-01-01T00:00:00Z: []
    #}

    # Test case:
    t = t_xy()
    t[t0] = XyPointCurve(PointList([Point(20.0, 96.0)]))
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: [(20.000000,96.000000)]\n}'
    #Expected results:
    #{
    #        2000-01-01T00:00:00Z: [(20.000000,96.000000)]
    #}

    # Test case:
    t = t_xy()
    t[t0] = XyPointCurve(PointList([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)]))
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: [(20.000000,96.000000),(40.000000,98.000000),(60.000000,99.000000),(80.000000,98.000000)]\n}'
    # Expected results:
    #{
    #        2000-01-01T00:00:00Z: [(20.000000,96.000000),(40.000000,98.000000),(60.000000,99.000000),(80.000000,98.000000)]
    #}

def test_t_xyz():

    # Test case:
    t = t_xyz()
    t[t0] = XyPointCurveWithZ(XyPointCurve(PointList([])),70.0)
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: z70.000000 []\n}'
    # Expected results:
    #{
    #        2000-01-01T00:00:00Z: z70.000000 []
    #}

    # Test case:
    t = t_xyz()
    t[t0] = XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0)])),70.0)
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: z70.000000 [(20.000000,70.000000)]\n}'
    # Expected results:
    #{
    #        2000-01-01T00:00:00Z: z70.000000 [(20.000000,70.000000)]
    #}

    # Test case:
    t = t_xyz()
    t[t0] = XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0)
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: z70.000000 [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)]\n}'
    # Expected results:
    #{
    #        2000-01-01T00:00:00Z: z70.000000 [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)]
    #}

def test_t_turbine_description():

    # Test case:
    t = t_turbine_description()
    str(t)
    print(t)
    assert str(t) == '{}'
    # Expected results:
    #{}

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription()
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: {}\n}'
    # Expected results:
    #{
    #        2000-01-01T00:00:00Z: {}
    #}

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([]))]))
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: {[]}\n}'
    # Expected results:
    #{
    #        2000-01-01T00:00:00Z: {[]}
    #}

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([])),70.0)]))]))
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: {\n\t\tz@70.000000: []\n\t}\n}'
    # Expected results:
    #{
    #        2000-01-01T00:00:00Z: {
    #                z@70.000000: []
    #        }
    #}

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))]))
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: {\n\t\tz@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]\n\t}\n}'
    # Expected result:
    #{
    #        2000-01-01T00:00:00Z: {
    #                z@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]
    #        }
    #}

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))]))
    t[t1] = TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))]))
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: {\n\t\tz@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]\n\t},\n\t2022-01-01T00:00:00Z: {\n\t\tz@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]\n\t}\n}'
    # Expected result:
    #{
    #        2000-01-01T00:00:00Z: {
    #                z@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]
    #        },
    #        2022-01-01T00:00:00Z: {
    #                z@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]
    #        }
    #}

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))]))
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: {\n\t\tz@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],\n\t\tz@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]\n\t}\n}'
    # Expected result:
    #{
    #        2000-01-01T00:00:00Z: {
    #                z@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],
    #                z@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]
    #        }
    #}

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))]))
    t[t1] = TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))]))
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: {\n\t\tz@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],\n\t\tz@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]\n\t},\n\t2022-01-01T00:00:00Z: {\n\t\tz@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],\n\t\tz@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]\n\t}\n}'
    # Expected result:
    #{
    #        2000-01-01T00:00:00Z: {
    #                z@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],
    #                z@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]
    #        },
    #        2022-01-01T00:00:00Z: {
    #                z@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],
    #                z@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]
    #        }
    #}

def test_t_turbine_description_for_pelton():

    #
    # Turbin description for pelton turbin, with needle combinations - more than one efficiency object!
    #

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0)])),TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0)]))]))
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: {\n\t\t1: {\n\t\t\tmin@0.000000,\n\t\t\tmax@0.000000,\n\t\t\tz@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)]\n\t\t},\n\t\t2: {\n\t\t\tmin@0.000000,\n\t\t\tmax@0.000000,\n\t\t\tz@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)]\n\t\t}\n\t}\n}'
    #{
    #        2000-01-01T00:00:00Z: {
    #                1: {
    #                        min@0.000000,
    #                        max@0.000000,
    #                        z@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)]
    #                },
    #                2: {
    #                        min@0.000000,
    #                        max@0.000000,
    #                        z@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)]
    #                }
    #        }
    #}

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)])),TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))]))
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: {\n\t\t1: {\n\t\t\tmin@0.000000,\n\t\t\tmax@0.000000,\n\t\t\tz@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],\n\t\t\tz@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]\n\t\t},\n\t\t2: {\n\t\t\tmin@0.000000,\n\t\t\tmax@0.000000,\n\t\t\tz@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],\n\t\t\tz@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]\n\t\t}\n\t}\n}'
    # Expected result:
    #{
    #        2000-01-01T00:00:00Z: {
    #                1: {
    #                        min@0.000000,
    #                        max@0.000000,
    #                        z@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],
    #                        z@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]
    #                },
    #                2: {
    #                        min@0.000000,
    #                        max@0.000000,
    #                        z@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],
    #                        z@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]
    #                }
    #        }
    #}

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)])),TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))]))
    t[t1] = TurbineDescription(TurbineEfficiencyList([TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)])),TurbineEfficiency(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),70.0),XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),72.0)]))]))
    str(t)
    print(t)
    assert str(t) == '{\n\t2000-01-01T00:00:00Z: {\n\t\t1: {\n\t\t\tmin@0.000000,\n\t\t\tmax@0.000000,\n\t\t\tz@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],\n\t\t\tz@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]\n\t\t},\n\t\t2: {\n\t\t\tmin@0.000000,\n\t\t\tmax@0.000000,\n\t\t\tz@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],\n\t\t\tz@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]\n\t\t}\n\t},\n\t2022-01-01T00:00:00Z: {\n\t\t1: {\n\t\t\tmin@0.000000,\n\t\t\tmax@0.000000,\n\t\t\tz@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],\n\t\t\tz@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]\n\t\t},\n\t\t2: {\n\t\t\tmin@0.000000,\n\t\t\tmax@0.000000,\n\t\t\tz@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],\n\t\t\tz@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]\n\t\t}\n\t}\n}'
    # Expected result:
    #{
    #        2000-01-01T00:00:00Z: {
    #                1: {
    #                        min@0.000000,
    #                        max@0.000000,
    #                        z@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],
    #                        z@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]
    #                },
    #                2: {
    #                        min@0.000000,
    #                        max@0.000000,
    #                        z@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],
    #                        z@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]
    #                }
    #        },
    #        2022-01-01T00:00:00Z: {
    #                1: {
    #                        min@0.000000,
    #                        max@0.000000,
    #                        z@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],
    #                        z@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]
    #                },
    #                2: {
    #                        min@0.000000,
    #                        max@0.000000,
    #                        z@70.000000: [(20.000000,70.000000),(40.000000,85.000000),(60.000000,92.000000),(80.000000,94.000000),(100.000000,92.000000),(110.000000,90.000000)],
    #                        z@72.000000: [(22.000000,72.000000),(42.000000,82.000000),(62.000000,92.000000),(82.000000,92.000000),(102.000000,92.000000),(112.000000,92.000000)]
    #                }
    #        }
    #}
