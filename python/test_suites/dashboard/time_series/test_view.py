import pytest
from shyft.dashboard.time_series.view import BaseView, FigureView
from shyft.dashboard.time_series.renderer import BaseFigureRenderer


def test_figure_view():
    figure_view1 = FigureView(view_container=1, color='blue', visible=True, label='Katze',
                              renderer_class=BaseFigureRenderer, unit="MW")
    figure_view2 = FigureView(view_container=1, color='blue', visible=True, label='Katze',
                              renderer_class=BaseFigureRenderer, unit="MW")

   #with pytest.raises(AttributeError):
   #    figure_view2.view_container_uid = 2#

    assert figure_view1 != figure_view2

    d = {figure_view1: 2}  # will not work as dict literal
    d[figure_view2] = 3

    d[figure_view1] = 10

    for k in d.keys():
        assert isinstance(k, BaseView)
        assert isinstance(k, FigureView)

    assert d[figure_view1] == 10

    assert figure_view2 in d

    assert 3 == d.pop(figure_view2)


