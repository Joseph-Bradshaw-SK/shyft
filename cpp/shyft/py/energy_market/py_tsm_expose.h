#pragma once
#include <string_view>
#include <string>
#include <stdexcept>
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/doc_macros.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/py/energy_market/py_attr_wrap.h>

namespace expose{
    namespace py = boost::python;
    using shyft::time_series::dd::apoint_ts;
    using shyft::energy_market::sbi_t;
    template<class PyC>
    PyC expose_tsm(PyC &c) {
        using w_tp=typename PyC::wrapped_type; // if py::class_<T> then wrapped type is T
        c.def_readwrite("ts",&w_tp::tsm,"Map keeping any extra time series for this object.");
        c.def("get_tsm_object",
                +[](w_tp& self, std::string key) {
                    auto it = self.tsm.find(key);
                    if (it == self.tsm.end()) throw std::runtime_error("Key does not exist");
                    return a_wrap<apoint_ts>([&self](sbi_t&so,int levels,int template_levels,std::string_view){
                            if(levels) self.generate_url(so,levels-1,template_levels>0?template_levels-1:template_levels);
                    }, std::string("ts.") + key, it->second);
                },
                (py::arg("self"),py::arg("key")),
                doc_intro("Get a specific extra time series for this object.")
                doc_details("The returned time series is wrapped in an object which exposes method for retrieving url etc.")
                doc_parameters()
                doc_parameter("key", "str", "The key in the tsm of the time series to get.")
                doc_raises()
                doc_raise("runtime_error", "If specified key does not exist.")
            );
        return c;
    };
}
