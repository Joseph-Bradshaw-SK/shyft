#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {
    namespace hana = boost::hana;
    namespace mp = shyft::mp;
    
    using std::static_pointer_cast;
    using std::dynamic_pointer_cast;
    using std::make_shared;
    using std::runtime_error;

    void contract::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels) const {
        if (levels) {
            auto tmp = sys_();
            if (tmp) tmp->generate_url(rbi, levels-1, template_levels ? template_levels - 1 : template_levels);
        }
        if (!template_levels) {
            constexpr std::string_view a = "/c{o_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto a="/c"+std::to_string(id);
            std::copy(std::begin(a), std::end(a), rbi);
        }
    }

    bool contract::operator==(contract const&o) const {
        if(this==&o) return true;//equal by addr.
        // compare power_plants
        auto pp_equal= stm::equal_attribute(power_plants,o.power_plants);

        return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use this that seems to be robust cross platform construct
            mp::leaf_accessors(hana::type_c<contract>),
            pp_equal && super::operator==(o),//initial value of the fold
            [this, &o](bool s, auto&& a) {
                return s?stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(o, a)):false; // only evaluate equal if the fold state is still true
            }
        );
    }

    contract_ contract::shared_from_this() const {
        auto s = sys_();
        if (s) {
            for (auto const& c : s->contracts)
                if(c.get()==this) return c;
        }
        return nullptr;
    }

    vector<contract_portfolio_> contract::get_portfolios() const {
        vector<contract_portfolio_> result;
        auto s = sys_();
        if(s) {
            for (auto const& cp : s->contract_portfolios) {
                for (auto const& c : cp->contracts) {
                    if (c.get() == this) {
                        result.push_back(cp);
                    }
                }
            }
        }
        return result;
    }
    void contract::add_to_portfolio(contract_portfolio_ portfolio) const {
        auto s = sys_();
        if(s) {
            auto me = shared_from_this();
            if (!me) throw std::runtime_error("this contract is not associated with a stm system");
            portfolio->contracts.push_back(me);
        }
    }

    vector<energy_market_area_> contract::get_energy_market_areas() const {
        vector<energy_market_area_> result;
        auto s = sys_();
        if(s) {
            for (auto const& ema : s->market) {
                for (auto const& c : ema->contracts) {
                    if (c.get() == this) {
                        result.push_back(ema);
                    }
                }
            }
        }
        return result;
    }
    void contract::add_to_energy_market_area(energy_market_area_ market) const {
        auto s = sys_();
        if(s) {
            auto me = shared_from_this();
            if (!me) throw std::runtime_error("this contract is not associated with a stm system");
            market->contracts.push_back(me);
        }
    }

}
