from shyft.energy_market.stm.utilities import create_t_turbine_description, t_turbine_description
from shyft.time_series import time, deltahours, deltaminutes, Calendar, TimeAxis, TimeSeries, POINT_AVERAGE_VALUE as stair_case
from shyft.energy_market.core import Point, PointList, XyPointCurve, XyPointCurveList, XyPointCurveWithZ, XyPointCurveWithZList, TurbineEfficiency, TurbineEfficiencyList, TurbineDescription

import pytest

shop = pytest.importorskip("shyft.energy_market.stm.shop")
if not shop.shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)


def create_test_optimization_commands(run_id: int, write_files: bool) -> shop.ShopCommandList:
    r = shop.ShopCommandList()
    if write_files: r.append(
        shop.ShopCommand.log_file(f"shop_log_{run_id}.txt")
    )
    r.extend([
        shop.ShopCommand.set_method_primal(),
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.start_sim(3),
        shop.ShopCommand.set_universal_mip_on(),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3)
    ])
    if write_files: r.extend([
        shop.ShopCommand.return_simres(f"shop_results_{run_id}.txt"),
        shop.ShopCommand.save_series(f"shop_series_{run_id}.txt"),
        shop.ShopCommand.save_xmlseries(f"shop_series_{run_id}.xml"),
        shop.ShopCommand.return_simres_gen(f"shop_genres_{run_id}.xml")
    ])
    return r


def test_stm_shop_forbidden_range(system_to_optimize):
    """verify shop optimization"""
    mega = 1000000
    stm_system = system_to_optimize
    commands = create_test_optimization_commands(1, False)
    t_begin = time('2018-10-17T10:00:00Z')
    t_end = time('2018-10-18T10:00:00Z')
    t_step = deltahours(1)
    n_steps = (t_end - t_begin)/t_step
    ta = TimeAxis(t_begin.seconds, t_step.seconds, n_steps.seconds)

    u1 = stm_system.hydro_power_systems[0].units[0]
    r1 = stm_system.hydro_power_systems[0].reservoirs[0]
    m1 = stm_system.market_areas[0]
    m1.price.value.set(3,36.4/mega)
    m1.price.value.set(20,100/mega)
    m1.max_sale.value.set(1, 20.0*mega)
    m1.max_sale.value.set(2, 25.0*mega)
    m1.max_sale.value.set(3, 29.0*mega)
    m1.max_sale.value.set(4, 31.0*mega)

    r1.level.realised = TimeSeries(ta,90.0,stair_case)
    xyz = XyPointCurveWithZ(XyPointCurve([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)]), 70.0)

    a1 = TurbineEfficiency([xyz], 20*mega, 22*mega, 22*mega, 20*mega, 22*mega)
    a2 = TurbineEfficiency([xyz], 24*mega, 30*mega, 30*mega, 24*mega, 30*mega)
    a3 = TurbineEfficiency([xyz], 51*mega, 80*mega, 80*mega, 50*mega, 80*mega)
    td = TurbineDescription([a1, a2, a3])
    t_td = t_turbine_description()
    t_td[t_begin] = td
    u1.turbine_description.value = t_td

    # Optimize with fixed step arguments
    shop_sys = shop.ShopSystem(ta)  # stm_system, ta, commands, False, False)
    shop_sys.set_logging_to_stdstreams(False)
    shop_sys.emit(stm_system)
    shop_sys.command(commands)
    shop_sys.collect(stm_system)
    #logs = shop_sys.get_log_buffer()

    assert u1.production.result.exists
    assert 18*mega < u1.production.result.value.values[0] < 80*mega
    #print(u1.production.result.value.values)
    # the results show shop generating production in all the three valid areas.
    # if we use the original turbine description, there will be production in the 'illegal' areas
    # thus, this demonstrates that shop takes into consideration the areas specified.
    expected_u1_prod=[
        19999989.05803541,
        19999989.17326578,
        23999970.9811971,
        19999989.83668149,
        23999972.4049289,
        50999871.77512709,
        50999869.06585581,
        50999853.07273478,
        50999939.517454274,
        51000145.07262535,
        51000538.781450875,
        51001180.66084656,
        51002132.35256962,
        51003457.24804266,
        51005220.54074063,
        51007489.452138245,
        51010333.81354035,
        51013826.27851278,
        51018042.55514603,
        51023061.66086718,
        69328163.32525633,
        53593935.861277506,
        53257261.80651038,
        52920702.5981522]

    assert  u1.production.result.value.values == pytest.approx(expected_u1_prod)
