#pragma once
#include <string>
#include <iosfwd>
#include <functional>
#include <shyft/energy_market/stm/shop/shop_log_entry.h>

namespace shyft::energy_market::stm::shop {

/** shop_logger that just reflects the sintef-shop interface/messages */
struct shop_logger {
    virtual void info(const char* msg) = 0;
    virtual void warning(const char* msg) = 0;
    virtual void error(const char* msg) = 0;
    virtual void enter() = 0; // NOTE: There is not a callback from Shop API matching this, but implementers can typically call it from constructor.
    virtual void exit() = 0;  // NOTE: It seems the Shop API never calls the matching callback, but implementers can call it from destructor.
    virtual ~shop_logger() {};
};

struct shop_memory_logger : shop_logger {
    std::vector<shop_log_entry> messages;
    shop_memory_logger() { messages.reserve(1000); }
    void info(const char* msg) override { messages.emplace_back(shop_log_entry::log_severity::information, msg); }
    void warning(const char* msg) override { messages.emplace_back(shop_log_entry::log_severity::warning, msg); }
    void error(const char* msg) override { messages.emplace_back(shop_log_entry::log_severity::error, msg); }
    void enter() override { /* os << "[" << id << "] [ENTER]" << std::endl; */ }
    void exit() override { /* os << "[" << id << "] [EXIT]" << std::endl; */ }
};

struct shop_stream_logger : shop_logger {
    std::string id;
    std::ostream& os;
    shop_stream_logger(std::string id, std::ostream& os);
    void info(const char* msg) override;
    void warning(const char* msg) override;
    void error(const char* msg) override;
    void enter() override;
    void exit() override;
};

}