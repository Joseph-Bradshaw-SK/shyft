from typing import List, Optional

import numpy as np

import pytest

from shyft.dashboard.base.ports import States
from shyft.dashboard.time_series.axes_handler import BokehViewTimeAxis, TimeAxisHandler
from shyft.dashboard.time_series.view_time_axes import create_view_time_axis, period_union
from shyft.time_series import (Calendar, UtcPeriod, TimeAxis, TimeAxisType, time, UtcTimeVector,
                               time_axis_extract_time_points_as_utctime)


def test_bokeh_view_time_axis(mock_bokeh_document, mock_parent):
    init_view_range = UtcPeriod(0, 1)
    bokeh_view = BokehViewTimeAxis(bokeh_document=mock_bokeh_document,
                                   init_view_range=init_view_range,
                                   zoom_in_interval=1,
                                   zoom_out_interval=1)
    bokeh_view.bind(parent=mock_parent)
    assert mock_parent.count_time_axis_update == 0
    assert bokeh_view._view_range == UtcPeriod(0, 1)
    bokeh_view._state = States.DEACTIVE
    bokeh_view.shared_x_range.end = 5000
    assert bokeh_view._current_view_range == UtcPeriod(0, 1)
    bokeh_view._state = States.ACTIVE
    bokeh_view.shared_x_range.end = 10000
    assert bokeh_view._current_view_range == UtcPeriod(0, 10)

    bokeh_view.set_view_range(UtcPeriod(0, 100), callback=True, padding=True)
    assert mock_parent.count_time_axis_update == 1
    assert bokeh_view.view_range == UtcPeriod(-5, 105)

    bokeh_view.set_view_range(UtcPeriod(0, 100), callback=True, padding=False)
    assert mock_parent.count_time_axis_update == 2
    assert bokeh_view.view_range == UtcPeriod(0, 100)

    bokeh_view.set_view_range(UtcPeriod(0, 100), callback=False, padding=False)
    assert mock_parent.count_time_axis_update == 2
    assert bokeh_view.view_range == UtcPeriod(0, 100)

    bokeh_view.x_range_data_updater()
    assert mock_parent.count_time_axis_update == 3
    assert bokeh_view._current_view_range is None


def test_bokeh_view_time_axis_utc_offset(mock_bokeh_document):
    init_view_range = UtcPeriod(0, 1)
    time_zone = 'Europe/Oslo'
    bokeh_view = BokehViewTimeAxis(bokeh_document=mock_bokeh_document,
                                   init_view_range=init_view_range,
                                   zoom_in_interval=1,
                                   zoom_out_interval=1,
                                   time_zone=time_zone)
    assert bokeh_view.utc_offset > 0
    assert bokeh_view.view_range == init_view_range
    assert int(bokeh_view.shared_x_range.start/bokeh_view.sec_to_milli) == int(init_view_range.start + bokeh_view.utc_offset)
    assert int(bokeh_view.shared_x_range.end/bokeh_view.sec_to_milli) == int(init_view_range.end + bokeh_view.utc_offset)

    t0 = Calendar(time_zone).time(2019, 1, 1, 0, 0)
    new_view_range = UtcPeriod(t0, t0 + 100)
    bokeh_view.set_view_range(new_view_range, callback=False, padding=False)
    assert bokeh_view.view_range == new_view_range
    assert int(bokeh_view.shared_x_range.start/bokeh_view.sec_to_milli) == int(new_view_range.start + bokeh_view.utc_offset)
    assert int(bokeh_view.shared_x_range.end/bokeh_view.sec_to_milli) == int(new_view_range.end + bokeh_view.utc_offset)


def test_time_axis_handler_view_range(mock_parent, mock_bokeh_document):
    hour = Calendar.HOUR
    day = Calendar.DAY
    init_view_range = UtcPeriod(0, 1)
    bokeh_view = BokehViewTimeAxis(bokeh_document=mock_bokeh_document,
                                   init_view_range=init_view_range,
                                   zoom_in_interval=1,
                                   zoom_out_interval=1)
    time_axis_handler = TimeAxisHandler(view_time_axis=bokeh_view, auto_dt_multiple=hour)
    time_axis_handler.auto_dt_figure_width = 10

    time_axis_handler.bind(parent=mock_parent)
    assert mock_parent.count_data_update == 0

    time_axis_handler.set_default_view_period(min_max_range=UtcPeriod(0, 1), reset_view=True)
    assert mock_parent.count_data_update == 0
    assert time_axis_handler.default_view_range == UtcPeriod(0, 1)

    time_axis_handler.set_view_range(view_range=UtcPeriod(1, 2), callback=True)
    # TODO: look into the double data update, one in the send_dt_options and one manual in trigger_time_axis_update()
    assert mock_parent.count_data_update == 2
    assert time_axis_handler.view_time_axis.view_range == UtcPeriod(1, 2)

    time_axis_handler.set_view_range(view_range=UtcPeriod(2, 3), callback=False)
    assert mock_parent.count_data_update == 2
    assert time_axis_handler.view_time_axis.view_range == UtcPeriod(2, 3)
    assert time_axis_handler.view_range == UtcPeriod(1, 2)

    time_axis_handler.trigger_time_axis_update()
    # TODO: another doubling for the same reason as above
    assert mock_parent.count_data_update == 4
    assert time_axis_handler.view_range == UtcPeriod(2, 3)

    time_axis_handler.trigger_time_axis_update()
    assert mock_parent.count_data_update == 5
    assert time_axis_handler.view_range == UtcPeriod(2, 3)

    sec_to_milli = bokeh_view.sec_to_milli

    def get_padded(start, end):
        padding = (end - start)*bokeh_view.x_rang_pad
        padded_start = int(round(int(round(start - padding)*sec_to_milli)/sec_to_milli))
        padded_end = int(round(int(round(end + padding)*sec_to_milli)/sec_to_milli))
        return padded_start, padded_end

    view_start = 0
    view_end = 14*hour
    time_axis_handler.set_view_range(view_range=UtcPeriod(view_start, view_end), callback=True)
    # TODO: another doubling for the same reason as above
    assert mock_parent.count_data_update == 7
    start, end = get_padded(view_start, view_end)
    assert time_axis_handler.view_range == UtcPeriod(start, end)
    assert time_axis_handler.view_range == UtcPeriod(start, end)
    assert time_axis_handler.auto_dt == 2*hour
    assert time_axis_handler.pad == day*7


def test_time_axis_handler_dt_restriction(mock_parent, mock_bokeh_document):
    hour = Calendar.HOUR
    day = Calendar.DAY
    init_view_range = UtcPeriod(0, 1)
    bokeh_view = BokehViewTimeAxis(bokeh_document=mock_bokeh_document,
                                   init_view_range=init_view_range,
                                   zoom_in_interval=1,
                                   zoom_out_interval=1)
    time_axis_handler = TimeAxisHandler(view_time_axis=bokeh_view,
                                        auto_dt_multiple=hour, time_step_restrictions=[day])
    time_axis_handler.auto_dt_figure_width = 10

    time_axis_handler.bind(parent=mock_parent)

    assert len(time_axis_handler.dt_options) == 0
    time_axis_handler.set_view_range(view_range=UtcPeriod(0, 22*hour), callback=True)
    assert len(time_axis_handler.dt_options) == 1
    assert time_axis_handler.dt_options[0] == 86400
    assert time_axis_handler.auto_dt == day


def test_time_axis_handler_zoom_pan(mock_parent, mock_bokeh_document):
    # TODO: Not complete!
    hour = Calendar.HOUR
    day = Calendar.DAY
    init_view_range = UtcPeriod(0, 1)
    bokeh_view = BokehViewTimeAxis(bokeh_document=mock_bokeh_document,
                                   init_view_range=init_view_range,
                                   zoom_in_interval=1,
                                   zoom_out_interval=1)
    time_axis_handler = TimeAxisHandler(view_time_axis=bokeh_view, auto_dt_multiple=hour)
    time_axis_handler.auto_dt_figure_width = 10
    time_axis_handler.bind(parent=mock_parent)
    assert time_axis_handler.pad == 0
    time_axis_handler.set_view_range(view_range=UtcPeriod(0, 2), callback=True)
    assert time_axis_handler.pad == 604800
    time_axis_handler.set_view_range(view_range=UtcPeriod(0, 3*day), callback=True)
    assert time_axis_handler.pad == 604800
    # zoom_threshold_factor = time_axis_handler.zoom_threshold_factor
    # pan_threshold_factor = time_axis_handler.pan_threshold_factor
    # time_axis_handler.pad = 1
    # time_axis_handler.zoom_threshold_factor = -1
    # time_axis_handler.pan_threshold_factor = 1
    # time_axis_handler.view_start_padded = 0
    # time_axis_handler.view_end_padded = 2
    # time_axis_handler.auto_dt = 3600
    # print(time_axis_handler.auto_dt)
    # print(time_axis_handler._calculate_auto_dt())
    # time_axis_handler.set_view_range(view_start=0, view_end=2, callback=True)
    # print(time_axis_handler.auto_dt)
    # print(time_axis_handler._calculate_auto_dt())
    # time_axis_handler.evaluate_view_range()



def compute_suitable_ticks(*, ta: TimeAxis, wanted_ticks: int, cal: Calendar, iso_year_week: bool, use_ta_points_if_possible: Optional[bool] = True) -> List[time]:
    """ TODO: Promote this when complete time-axis label and handling is ready
    Compute suitable number of `wanted_ticks` for a specified time-axis `ta`, using
    `cal` to compute calendar boundaries in either Y-M-D or iso Y-W-day coordinates

    :param ta: time-axis for which you would like to have suitable ticks
    :param wanted_ticks: number of ticks wanted
    :param cal: presentation time-zone calendar, e.g. calendar resolution tickers will be rounded to this calendar tz
    :param iso_year_week: use ISO year,week,day regime in stead of year,month,day
    :param use_ta_points_if_possible: if time-axis points <= wanted_points , just return the ticks including end-point of the time-axis
    :return:
    """
    if wanted_ticks < 1:
        raise RuntimeError(f"parameter wanted_ticks must be >=1: was {wanted_ticks}")
    if len(ta) == 0:
        return UtcTimeVector([])
    if len(ta) < wanted_ticks and use_ta_points_if_possible:
        return time_axis_extract_time_points_as_utctime(ta)
    total_period = ta.total_period()
    span = total_period.timespan()
    x_steps = [  # table driven approach, using base-unit and multiples of those
        (Calendar.YEAR, [1, 2, 5, 10, 100, 1000, 10000]),
        (Calendar.WEEK, [1, 2, 4, 10]) if iso_year_week else (Calendar.MONTH, [1, 2, 3, 4, 6]),
        (Calendar.DAY, [1] if iso_year_week else [1, 2, 3, 5, 10]),
        (time(3600), [1, 2, 3, 6, 12]),
        (time(60), [1, 2, 3, 5, 10, 15, 30]),
        (time(1), [1, 2, 3, 5, 10, 15, 30]),
        (time(0.1), [1, 2, 5]),
        (time(0.01), [1, 2, 5]),
        (time(0.001), [1, 2, 5]),
        (time(0.0001), [1, 2, 5]),
        (time(0.00001), [1, 2, 5]),
        (time(0.000001), [1, 2, 5]),
    ]
    for i, (base_dt, x_mult) in enumerate(x_steps):
        base_ticks = span/base_dt
        if base_ticks >= wanted_ticks:
            break
        # we got minimum required ticks:
    x = 1
    for m in x_mult[1:]:
        m_ticks = int(span/(m*base_dt))
        if m_ticks >= wanted_ticks:
            x = m
        else:
            break
    cal = cal or Calendar()
    t = cal.trim(ta.time(0), time(base_dt))
    r = UtcTimeVector()
    while t < total_period.end:
        r.append(t)
        t = cal.add(t, base_dt, x)
    return r


def test_compute_suitable_ticks_seconds():
    c = Calendar()
    assert len(compute_suitable_ticks(ta=TimeAxis(), wanted_ticks=5, cal=c, iso_year_week=False)) == 0, 'expect zero ticks for zero length time-axis'
    with pytest.raises(RuntimeError):
        compute_suitable_ticks(ta=TimeAxis(time(0.2), time(1), 10), wanted_ticks=0, cal=c, iso_year_week=False)

    # sub-seconds resolutionns
    for f in [1, 10, 100, 1000, 10000, 100000, 1000000]:
        assert UtcTimeVector([time(x/f) for x in range(0, 10, 2)]) == compute_suitable_ticks(ta=TimeAxis(time(0.0), time(1/f), 10), wanted_ticks=5, cal=c, iso_year_week=False), f'Failed sub-seconds case {1/f}'
    assert UtcTimeVector([time(x) for x in range(0, 12, 2)]) == compute_suitable_ticks(ta=TimeAxis(time(0.2), time(1), 10), wanted_ticks=5, cal=c, iso_year_week=False)
    assert UtcTimeVector([time(x) for x in range(0, 11, 1)]) == compute_suitable_ticks(ta=TimeAxis(time(0), time(1), 10), wanted_ticks=15, cal=c, iso_year_week=False), 'Expect time-axis points if wanted-points is <= time-axis'
    assert UtcTimeVector([time(x)/2 for x in range(0, 2*3, 1)]) == compute_suitable_ticks(ta=TimeAxis(time(0), time(1), 3), wanted_ticks=5, cal=c, use_ta_points_if_possible=False, iso_year_week=False), 'Expect that override time-axis points work'


def test_compute_suitable_ticks_minutes():
    oslo = Calendar('Europe/Oslo')
    ticks = compute_suitable_ticks(ta=TimeAxis(time(0), time(60), 15), wanted_ticks=5, cal=oslo, iso_year_week=False)
    assert UtcTimeVector([time(x*60) for x in range(0, 15, 3)]) == ticks
    ticks = compute_suitable_ticks(ta=TimeAxis(time(-6), time(60), 15), wanted_ticks=5, cal=oslo, iso_year_week=False)
    assert UtcTimeVector([time(x*60) for x in range(-1, 15, 3)]) == ticks

    ticks = compute_suitable_ticks(ta=TimeAxis(time(-6), time(60/2),2* 15), wanted_ticks=5, cal=oslo, iso_year_week=False)
    assert UtcTimeVector([time(x*60) for x in range(-1, 15, 3)]) == ticks


def test_compute_suitable_ticks_hours():
    oslo = Calendar('Europe/Oslo')
    ticks = compute_suitable_ticks(ta=TimeAxis(time(0), time(3600), 15), wanted_ticks=5, cal=oslo, iso_year_week=False)
    assert UtcTimeVector([time(x*3600) for x in range(0, 15, 3)]) == ticks
    ticks = compute_suitable_ticks(ta=TimeAxis(time(0), time(3600*3), 5), wanted_ticks=4, cal=oslo, iso_year_week=False)
    assert UtcTimeVector([time(x*3600) for x in range(0, 15, 3)]) == ticks

    ticks = compute_suitable_ticks(ta=TimeAxis(time(-600), time(3600), 15), wanted_ticks=5, cal=oslo, iso_year_week=False)
    assert UtcTimeVector([time(x*3600) for x in range(-1, 15, 3)]) == ticks

    ticks = compute_suitable_ticks(ta=TimeAxis(time(-600), time(3600/2),2* 15), wanted_ticks=5, cal=oslo, iso_year_week=False)
    assert UtcTimeVector([time(x*3600) for x in range(-1, 15, 3)]) == ticks


def test_compute_suitable_ticks_days():
    oslo = Calendar('Europe/Oslo')
    t0=oslo.time(2020,3,29,)
    ticks = compute_suitable_ticks(ta=TimeAxis(t0, time(3600), 24*3), wanted_ticks=5, cal=oslo, iso_year_week=False)
    assert UtcTimeVector([oslo.add(t0,time(12*3600),x) for x in range(7)]) == ticks
    ticks = compute_suitable_ticks(ta=TimeAxis(t0, time(3600), 24*3), wanted_ticks=24*3/3, cal=oslo, iso_year_week=False)
    e=UtcTimeVector()
    t=t0
    for i in range(24+1):
        e.append(t)
        t=oslo.add(t,3600,3)
    assert ticks == e

def test_compute_suitable_ticks_weeks():
    oslo = Calendar('Europe/Oslo')
    t0=oslo.time(2020,3,29,)
    ta = TimeAxis(t0, time(3600), 24*(7*7))
    ticks = compute_suitable_ticks(ta=ta, wanted_ticks=5, cal=oslo, iso_year_week=True)
    assert TimeAxis(oslo,oslo.time(2020,3,23),Calendar.WEEK,7) == TimeAxis(ticks)
    ta = TimeAxis(t0, time(3600*3), 24*(7*52*5))
    ticks = compute_suitable_ticks(ta=ta, wanted_ticks=15, cal=oslo, iso_year_week=True)
    for t in ticks:
        cc=oslo.calendar_week_units(t)
        assert cc.week_day==1 and cc.hour == 0 and cc.minute == 0 and cc.second==0

def test_compute_suitable_ticks_months():
    oslo = Calendar('Europe/Oslo')
    t0=oslo.time(2020,3,29,)
    ta = TimeAxis(t0, time(3600), 24*(7*52*5))
    ticks = compute_suitable_ticks(ta=ta, wanted_ticks=25, cal=oslo, iso_year_week=False)
    assert len(ticks)==31
    for t in ticks:
        cc=oslo.calendar_units(t)
        assert cc.day==1 and cc.hour == 0 and cc.minute == 0 and cc.second==0


def test_create_view_time_axis():
    cal = Calendar('Europe/Oslo')
    vp = UtcPeriod(cal.time(2020, 3, 28, 1, 2, 3), cal.time(2020, 4, 1, 3, 4, 5))
    cp = vp
    cp_special = UtcPeriod()

    result = create_view_time_axis(cal=cal, view_period=vp, clip_period=cp, dt=Calendar.DAY)
    expected = TimeAxis(cal, cal.time(2020, 3, 28), Calendar.DAY, 4)
    assert result == expected
    assert result.timeaxis_type == TimeAxisType.CALENDAR

    result = create_view_time_axis(cal=cal, view_period=vp, clip_period=cp_special, dt=Calendar.DAY)
    expected = TimeAxis(cal, cal.time(2020, 3, 28), Calendar.DAY, 4)
    assert result == expected
    assert result.timeaxis_type == TimeAxisType.CALENDAR

    # below day resolution it should be FixedDT
    result = create_view_time_axis(cal=cal, view_period=vp, clip_period=cp, dt=Calendar.HOUR)
    expected = TimeAxis(cal, cal.time(2020, 3, 28, 1), Calendar.HOUR, vp.diff_units(cal, cal.HOUR))
    assert result == expected
    assert result.timeaxis_type == TimeAxisType.FIXED

    # check with another clip period
    cp_3h = UtcPeriod(cal.time(2020, 3, 28, 12, 2, 3), cal.time(2020, 3, 28, 15, 4, 5))
    result = create_view_time_axis(cal=cal, view_period=vp, clip_period=cp_3h, dt=Calendar.HOUR)
    expected = TimeAxis(cal, cal.time(2020, 3, 28, 12), Calendar.HOUR, 3)
    assert result == expected
    assert result.timeaxis_type == TimeAxisType.FIXED

    # function works also for overlap < dt
    cp_small_overlap = UtcPeriod(vp.end - 3 * cal.HOUR, cal.time(2020, 5, 1))
    assert UtcPeriod.intersection(cp_small_overlap, cp).timespan() == 3 * cal.HOUR
    result = create_view_time_axis(cal=cal, view_period=vp, clip_period=cp_small_overlap, dt=Calendar.DAY)
    expected = TimeAxis(cal, cal.time(2020, 4, 1), Calendar.DAY, 1)
    assert result == expected
    assert result.timeaxis_type == TimeAxisType.CALENDAR

    # check returns empty calendar for some cases
    # invalid view period
    assert create_view_time_axis(cal=cal, view_period=UtcPeriod(), clip_period=vp, dt=cal.DAY) == TimeAxis()

    # invalid clip period is ignored
    result = create_view_time_axis(cal=cal, view_period=vp, clip_period=UtcPeriod(), dt=cal.DAY)
    expected = TimeAxis(cal, cal.time(2020, 3, 28), Calendar.DAY, 4)
    assert result == expected

    # no overlap
    assert create_view_time_axis(cal=cal, view_period=UtcPeriod(200, 400), clip_period=vp, dt=cal.DAY) == TimeAxis()

    # dt = 0
    assert create_view_time_axis(cal=cal, view_period=vp, clip_period=cp, dt=0) == TimeAxis()


cal = Calendar()
t0 = cal.time(2020, 12, 1)
t1 = cal.time(2021, 1, 1)
t2 = cal.time(2021, 2, 1)
t3 = cal.time(2021, 5, 1)
@pytest.mark.parametrize("p1, p2, expected",
                        [
                            [UtcPeriod(t0, t1), UtcPeriod(t1, t2), NotImplementedError],
                            [UtcPeriod(t0, t1), UtcPeriod(t2, t3), NotImplementedError],
                            [UtcPeriod(t0, t2), UtcPeriod(t1, t3), UtcPeriod(t0, t3)],
                            [UtcPeriod(t2, t1), UtcPeriod(t1, t2), ValueError],
                            [UtcPeriod(t0, t1), UtcPeriod(t0, t1), UtcPeriod(t0, t1)],
                            [UtcPeriod(t0, t3), UtcPeriod(t1, t2), UtcPeriod(t0, t3)]
                        ])
def test_period_union(p1, p2, expected):

    if isinstance(expected, UtcPeriod):
        assert period_union(p1, p2) == expected
    else:
        with pytest.raises(expected):
            period_union(p1, p2)