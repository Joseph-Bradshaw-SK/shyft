from shyft.energy_market.stm import HydroPowerSystem, StmSystem
from shyft.energy_market.core import ConnectionRole, Point, PointList, XyPointCurve, XyPointCurveWithZ
from shyft.energy_market.stm.utilities import create_t_double, create_t_turbine_description
from shyft.time_series import time, TimeAxis, TimeSeries, deltahours, POINT_AVERAGE_VALUE


def create_mock_hps(id: int=1, name: str="Hydro_power_system") -> HydroPowerSystem:
    """
    Test to learn how to model hydro power system with new stm-model in python
    """

    hps = HydroPowerSystem(1, "Hydro_power_system")
    t0 = time("2000-01-01T00:00:00Z")

    # ---------- Reservoir ----------
    rsv = hps.create_reservoir(1, "Reservoir")

    # Lowest regulated water level
    rsv.level.regulation_min = create_t_double(t0, 200.0)
    # Highest regulated water level
    rsv.level.regulation_max= create_t_double(250)

    # Volume vs. head:
    rsv.volume_level_mapping= XyPointCurve(PointList([
        Point(0.00000, 200.00000),
        Point(30.00000, 210.00000),
        Point(65.00000, 220.00000),
        Point(105.00000, 230.00000),
        Point(155.00000, 240.00000),
        Point(210.00000, 250.00000),
        Point(230.00000, 252.00000)]))

    # ------------- Units -------------
    unit1 = hps.create_unit(1, 'Unit1')

    unit1.turbine_description = create_t_turbine_description(t0, [
        XyPointCurveWithZ(XyPointCurve(PointList([
            Point(15.00000, 90.000000),
            Point(20.00000, 92.250000),
            Point(25.00000, 93.750000),
            Point(30.00000, 95.000000)
        ])), 225.000),

        XyPointCurveWithZ(XyPointCurve(PointList([
            Point(15.00000, 90.500000),
            Point(20.00000, 92.250000),
            Point(25.00000, 93.500000),
            Point(30.00000, 94.600000)
        ])), 240.000),
    ])

    unit1.production.static_min = create_t_double(t0, 30.0)
    unit1.production.static_max = create_t_double(t0, 60.0)
    unit1.production.nominal = create_t_double(t0, 60.0)

    unit1.cost.start = TimeSeries(
        TimeAxis(time('2010-01-01T00:00:00Z'), deltahours(8760), 25),
        fill_value=200.0, point_fx=POINT_AVERAGE_VALUE)
    unit1.cost.stop = TimeSeries(
        TimeAxis(time('2010-01-01T00:00:00Z'), deltahours(8760), 25),
        fill_value=200.0, point_fx=POINT_AVERAGE_VALUE)

    unit1.generator_description = XyPointCurve(PointList([
        Point(30.00000, 98.100000),
        Point(40.00000, 98.300000),
        Point(50.00000, 98.470000),
        Point(60.00000, 98.600000)
    ]))

    # ----------- Power plants -----------
    leirdola_pp = hps.create_power_plant(1, 'Unit1')
    leirdola_pp.add_unit(unit1)

    leirdola_pp.outlet_level = create_t_double(t0, 5.5)  # Outlet level might to be moved to waterway

    # ------------- Waterways -------------
    wr1 = hps.create_waterway(1, 'w_Reservoir_to_Unit1')
    pen1 = hps.create_waterway(2, 'Penstock 1 Unit1')
    tr1 = hps.create_waterway(3, 'Tail race for Unit1')

    wr1.input_from(rsv).output_to(pen1)
    pen1.input_from(wr1).output_to(unit1)
    tr1.input_from(unit1)

    wr1.head_loss_coeff.value = create_t_double(t0, 0.01)
    pen1.head_loss_coeff.value = create_t_double(t0, 0.0075)  # Also on tailrace?

    wr1.add_gate(1, 'w_Reservoir_to_Unit1_gate', '{}')

    hps.create_river(4, 'f_Reservoir_Sea') \
        .input_from(rsv, ConnectionRole.flood)

    hps.create_river(5, 'b_Reservoir_Sea') \
        .input_from(rsv, ConnectionRole.bypass)

    return hps

def create_filled_stm_model(id:int=1, name:str="stm_model"):
    mdl = StmSystem(id, name, json='')
    mdl.hydro_power_systems.append(create_mock_hps(id, f"{name} - HPS"))
    return mdl
