/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::hydrology::grammar {
    extern shyft::core::utcperiod parse_cf_time(const char*);
}

namespace expose {
    namespace py=boost::python;
    void cf_time() {
        using shyft::hydrology::grammar::parse_cf_time;
        py::def("parse_cf_time",&parse_cf_time,(py::arg("reference")),

           doc_intro("parses cf time unit string like hours since 1970-01-01 00:00:00")
           doc_intro("and returs an UtcPeriod, where the .start is the reference time")
           doc_intro("and the .timespan() is the delta time, e.g. 3600.0")
           doc_intro("If the reference is illformed, then the returned period is not .valid()")
           doc_intro("The intended usage is to get the reference time and time-delta of netcdf files,")
           doc_intro("replacing the need of cftime python library.")
           doc_parameters()
           doc_parameter("reference","str", "netcdf time-reference")
           doc_returns("p","UtcPeriod","p.valid()==True if well formed,p.start equal to the reference time, and p.timespan() equal to the time-delta specified")
        );
    }
}
