#pragma once
#include <string>
#include <vector>
#include <memory>
#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/power_plant.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using shyft::time_series::dd::apoint_ts;

    struct contract_portfolio;
    using contract_portfolio_ = shared_ptr<contract_portfolio>;

    /** @brief Contract
     *
     * Represents a financial or physical contract, optionally associated with
     * energy market area, power plant, or portfolio.
     */
    struct contract : id_base {
        using super = id_base;

        /** @brief Generate an almost unique, url-like string for this object.
         *
         * @param rbi Back inserter to store result.
         * @param levels How many levels of the url to include. Use value 0 to
         *     include only this level, negative value to include all levels (default).
         * @param template_levels From which level to start using placeholder instead of
         *     actual object ID. Use value 0 for all, negative value for none (default).
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

        contract() { mk_url_fx(this); }
        contract(int id, const string& name, const string& json, const stm_system_& sys)
            : super{id,name,json,{},{}},sys{sys} { mk_url_fx(this); }

        bool operator==(const contract& other) const;
        bool operator!=(const contract& other) const { return !(*this == other); }

        stm_system_ sys_() const { return sys.lock(); }
        stm_system__ sys; ///< Reference up to the 'owning' optimization system.
        contract_ shared_from_this() const; // Get shared pointer from this, via sys.

        BOOST_HANA_DEFINE_STRUCT(contract,
            (apoint_ts, quantity),      ///< Quantity (volume), e.g. W,J/s (maybe energy_flow is better?).
            (apoint_ts, price),         ///< Price per quantity unit, e.g. EUR/J.
            (apoint_ts, fee),           ///< Any contract fees, currency unit.
            (apoint_ts, revenue),       ///< Revenue, actual or forecast. Usually a calculated value, e.g. cash_flow = volume*price - fee [EUR/s].
            (string, parent_id),       ///< Optional reference to parent (contract) (maybe use weakptr?).
            (string, buyer),            ///< Name of the buyer actor (later a ref to actor).
            (string, seller),           ///< Name of the seller actor (later a ref to actor).
            (apoint_ts, active),        ///< Contract status (e.g. active, inactive).
            (apoint_ts, validation)     ///< Validation status (e.g. whether the contract has been validated, and at which level).
        );

        /** Get associated energy market areas. */
        vector<energy_market_area_> get_energy_market_areas() const;
        /** Associated with energy market area. */
        void add_to_energy_market_area(energy_market_area_ market) const;

        /** Get associated contract portfolios. */
        vector<contract_portfolio_> get_portfolios() const;
        /** Associate with contract portfolio. */
        void add_to_portfolio(contract_portfolio_ portfolio) const;

        vector<power_plant_> power_plants; ///< Association with power plants.

        x_serialize_decl();
    };

    using contract_ = shared_ptr<contract>;
}

x_serialize_export_key(shyft::energy_market::stm::contract);
