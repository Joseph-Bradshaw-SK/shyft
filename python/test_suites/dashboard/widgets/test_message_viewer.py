from shyft.dashboard.widgets.message_viewer import MessageViewer
import datetime
import pytest

def test_use_two_digits_for_all_clock_numbers():
    test = MessageViewer._format_text_with_time_prefix(datetime.datetime(2020, 1, 1, 1, 1, 1), 'Hello')

    assert test == '[01:01:01] - Hello'
