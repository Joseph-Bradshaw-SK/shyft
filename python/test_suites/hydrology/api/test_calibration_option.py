from pytest import approx

from shyft.hydrology import CalibrationOption, OptimizerMethod
from shyft.time_series import time


def test_basics():
    c = CalibrationOption()
    assert c.method == OptimizerMethod.BOBYQA
    assert c.max_iterations == 1500
    assert c.time_limit == 0
    assert c.solver_epsilon == approx(0.0001)
    assert c.x_epsilon == approx(0.0001)
    assert c.y_epsilon == approx(0.0001)
    assert c.tr_start == approx(0.1)
    assert c.tr_stop == approx(1e-5)


def test_construct_global_search():
    c = CalibrationOption.global_search(
        max_iterations=1200,
        time_limit=time(3500),
        solver_epsilon=0.01
    )
    assert c.method == OptimizerMethod.GLOBAL
    assert c.max_iterations == 1200
    assert c.time_limit == time(3500)
    assert c.solver_epsilon == approx(0.01)


def test_construct_all():
    c = CalibrationOption(method=OptimizerMethod.GLOBAL,
                          max_iterations=1200,
                          time_limit=time(3500),
                          solver_epsilon=0.01,
                          x_epsilon=0.02,
                          y_epsilon=0.03,
                          tr_start=0.2,
                          tr_stop=0.001
                          )
    assert c.method == OptimizerMethod.GLOBAL
    assert c.max_iterations == 1200
    assert c.time_limit == time(3500)
    assert c.solver_epsilon == approx(0.01)
    assert c.x_epsilon == approx(0.02)
    assert c.y_epsilon == approx(0.03)
    assert c.tr_start == approx(0.2)
    assert c.tr_stop == approx(0.001)


def test_construct_bobyqa():
    c = CalibrationOption.bobyqa(
        max_iterations=1200,
        tr_start=0.12,
        tr_stop=1e-3
    )
    assert c.method == OptimizerMethod.BOBYQA
    assert c.max_iterations == 1200
    assert c.tr_start == approx(0.12)
    assert c.tr_stop == approx(1e-3)


def test_construct_sceua():
    c = CalibrationOption.sceua(
        max_iterations=1200,
        x_epsilon=0.012,
        y_epsilon=0.022
    )
    assert c.method == OptimizerMethod.SCEUA
    assert c.max_iterations == 1200
    assert c.x_epsilon == approx(0.012)
    assert c.y_epsilon == approx(0.022)


def test_construct_dream():
    c = CalibrationOption.dream(
        max_iterations=1200,
    )
    assert c.method == OptimizerMethod.DREAM
    assert c.max_iterations == 1200
