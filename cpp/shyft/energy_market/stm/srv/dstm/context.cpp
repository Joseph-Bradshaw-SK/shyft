#include <shyft/energy_market/stm/srv/dstm/context.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm::srv::dstm {
    using shyft::core::utctime_now;

    bool stm_system_context::message(const string& msg) {
        //TODO: without proxy-attrs, - there is no notification.
        if (mdl) {
            vector<pair<utctime, string>> msgs;
            if (mdl->run_params.fx_log.size())
                msgs = mdl->run_params.fx_log;
            msgs.push_back({utctime_now(), msg});
            mdl->run_params.fx_log=msgs;
            return true;
        }
        return false;
    }
}
