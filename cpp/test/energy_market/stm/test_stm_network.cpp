#include <doctest/doctest.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/busbar.h>

using namespace shyft::energy_market::stm;
using std::make_shared;

namespace {
    struct test_sys {
        stm_system_ sys;
        network_ net;
        transmission_line_ t;
        busbar_ b;
        test_sys() {
            sys=make_shared<stm_system>(1,"sys","{}");
            net=make_shared<network>(2,"net","{}", sys);
            t = make_shared<transmission_line>(3, "t", "{}", net);
            b = make_shared<busbar>(4, "b", "{}", net);
            sys->networks.push_back(net);
            net->transmission_lines.push_back(t);
            net->busbars.push_back(b);
        }
    };
}

TEST_SUITE("stm_network") {

    TEST_CASE("network_construct") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto net=make_shared<network>(2, "net","{}", sys);
        FAST_CHECK_UNARY(net);
        FAST_CHECK_EQ(net->id,2);
        FAST_CHECK_EQ(net->name,"net");
        FAST_CHECK_EQ(net->json,"{}");
    }

    TEST_CASE("network_equality") {
        test_sys sys1;
        test_sys sys2;
        CHECK_EQ(*sys1.net, *sys1.net); // equal to self
        CHECK_EQ(*sys1.net, *sys2.net);    
    }
}
