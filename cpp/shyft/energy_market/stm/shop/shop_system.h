/*
 * Integration of stm with shop api via proxy.
 */
#pragma once
#include <memory>
#include <vector>
#include <iosfwd>
#include <shyft/energy_market/stm/shop/shop_logger.h>
#include <shyft/energy_market/stm/shop/shop_log_hook.h>
#include <shyft/energy_market/stm/shop/shop_adapter.h>
#include <shyft/energy_market/stm/shop/shop_emitter.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/shop/shop_commander.h>
#include <shyft/energy_market/stm/shop/export/shop_export.h>
#include <shyft/energy_market/stm/shop/shop_visitor.h>

namespace shyft::energy_market::stm::shop {

using namespace shyft::energy_market;
using namespace shyft::energy_market::stm;
using srv::dstm::stm_system_context;
using srv::dstm::model_state;
using shyft::core::to_seconds64;

/** @brief shop_system allows optimization  of a stm_system
 *  @details
 *  The class is responsible for facilitating and keeping the
 *  correlated state between the descriptive stm system model
 *  and the corresponding optimization shop-model.
 *  It delegates the work to shop_emitter, shop_adapter and shop_commander.
 *  The logger takes care of feedback from the shop-engine, and the
 *  shop_visitor takes care of event managment/notification.
 * 
 * 
 */
struct shop_system {
    shop_api api;             ///< Shop api proxy object, could be private, but is set public for testing.
    shop_adapter adapter;     ///< Basic stm to shop object adapter, takes care of 1-1 emitting tasks helper for emitter.
    shop_emitter emitter;     ///< The shop exchange interface, that is used to orchestrate shop, notice that emitter have refs to the above api and emitter.
    shop_commander commander; ///< Shop is a stateful live system and commander helps us interact with the instance.
    shop_visitor_ vis;        ///< Event uplink to the stm model.
private:
    shyft::time_axis::generic_dt time_axis; ///< Optimization time-axis.
    std::shared_ptr<shop_logger> logger;    ///< Adapts logs emitted from the shop engine

public:
    // Constructors
    shop_system(const shyft::time_axis::generic_dt& time_axis, stm_system_context* ctx=nullptr, string prefix="");
    ~shop_system();

    string get_version_info() {
        return api.get_version_info();
    }

    void set_logging_to_stdstreams(bool on = true) {
        api.set_logging_to_stdstreams(on);
    }

    void set_logging_to_files(bool on = true) {
        api.set_logging_to_files(on);
    }

    std::vector<shop_log_entry> get_log_buffer(int limit = 1024) {
        auto log_entries = api.get_log_buffer(limit);
        return std::vector<shop_log_entry>(log_entries.begin(), log_entries.end());
    }

    void emit(const stm_system& stm);                        ///< Emit stm system to shop, typical step 1 of an optimization.
    void command(std::vector<shop_command> const& commands); ///< Execute commands on shop, typical step 2 of an optimization.
    void collect(stm_system& stm);                           ///< Collect results from shop into stm system, typical step 3 of an optimization.
    void complete(stm_system& stm);                          ///< Update stm system, performing post-process calculations based on collected results from shop, typical step 4 of an optimization.

    // get some default/singleton shop objects not completely represented
    // in stm but may be relevant (temporary solution, to be refactored?)
    shop_global_settings get_global_settings();
    shop_objective get_average_objective();

    // export the entire system as yaml formatted string (using function from shop api)
    string export_yaml(bool input_only = false, bool compress_txy = false, bool compress_connection = false) {
        return api.dump_yaml(input_only, compress_txy, compress_connection);
    }
    // export topology graph as dot formatted string into stream (custom implementation)
    void export_topology(bool all, bool raw, std::ostream& destination) const;
    // export data as json formatted string into stream (custom implementation)
    void export_data(bool all, std::ostream& destination) const;

    // ease of use and utility functions
    //static void optimize(stm_system& stm, utctime t_begin, utctime t_end, utctimespan t_step, const std::vector<shop_command>& commands, bool logging_to_stdstreams, bool logging_to_files, string prefix="");
    static void optimize(stm_system& stm, const generic_dt& time_axis, const std::vector<shop_command>& commands, bool logging_to_stdstreams, bool logging_to_files, string prefix="");
    static void environment(std::ostream& destination);

    void install_logger(std::shared_ptr<shop_logger> logger);
    void uninstall_logger();
private:
    static void set_time_axis(shop_api& api, const utcperiod& period, const utctimespan& t_step);
    static void set_time_axis(shop_api& api, const shyft::time_axis::generic_dt& time_axis);

};

}
