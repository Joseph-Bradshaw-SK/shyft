#
#    CMake build system for Shyft open source
#
#    Based on work by John Eivind R. Helset, ref. to project https://gitlab.com/jehelset/hysj
#
#     Preconditions : 
#       (1) C++20 compliant compiler
#       (2) Successful build and available shyft_dependencies with required 3rd party libraries
#
#
#     The next environment variables are honored:
#
#        SHYFT_DEPENDENCIES_DIR: default ${PROJECT_SOURCE_DIR}/../shyft_dependencies
#
cmake_minimum_required(VERSION 3.19 FATAL_ERROR)

include(${CMAKE_CURRENT_LIST_DIR}/cmake/shyft_prologue.cmake) # find version/tag, os, set package info etc.

project(
    shyft
    LANGUAGES CXX
    VERSION ${SHYFT_VERSION_MAJOR}.${SHYFT_VERSION_MINOR}.${SHYFT_VERSION_PATCH}
    DESCRIPTION ${SHYFT_DESCRIPTION}
    HOMEPAGE_URL ${SHYFT_URL}
)
include(shyft_options)  # this configure which options to include, like hydrology, python energy market
include(shyft_config)   # setup compiler, find packages, set up install directories

add_subdirectory(cpp/shyft/core)  # this is the bare minimum, development package
add_subdirectory(cmake/config)    # needed to install the c++ dev package including cmake support


if(SHYFT_WITH_ENERGY_MARKET)
    if(SHYFT_WITH_SHOP)
        add_subdirectory(cpp/shyft/energy_market/stm/shop/api)
    endif()
    add_subdirectory(cpp/shyft/energy_market)
    add_subdirectory(cpp/shyft/energy_market/stm)
    add_subdirectory(cpp/shyft/energy_market/ui)
endif()

if(SHYFT_WITH_HYDROLOGY)
    add_subdirectory(cpp/shyft/hydrology/api)
endif()

if(SHYFT_WITH_PYTHON)
  add_subdirectory(cpp/shyft/py/time_series)
  if(SHYFT_WITH_HYDROLOGY)
    add_subdirectory(cpp/shyft/py/hydrology)
  endif()
  if(SHYFT_WITH_ENERGY_MARKET)
    add_subdirectory(cpp/shyft/py/energy_market/core)
    add_subdirectory(cpp/shyft/py/energy_market/ltm)
    add_subdirectory(cpp/shyft/py/energy_market/stm)
    add_subdirectory(cpp/shyft/py/energy_market/stm/shop)
    add_subdirectory(cpp/shyft/py/energy_market/ui)
  endif()
endif()

if(SHYFT_WITH_TESTS)
    enable_testing()
    include(CTest)
    add_subdirectory(cpp/test)
    
    if(SHYFT_WITH_ENERGY_MARKET)
        add_subdirectory(cpp/test/energy_market)
        add_subdirectory(cpp/test/energy_market/ui)
        if(SHYFT_WITH_SHOP)
            add_subdirectory(cpp/test/energy_market/shop)
        endif()
    endif()
    
    if(SHYFT_WITH_HYDROLOGY)
        add_subdirectory(cpp/test/hydrology)
    endif()    
endif()

if(SHYFT_WITH_COVERAGE)
    add_subdirectory(cpp/coverage)
endif()

include(${CMAKE_CURRENT_LIST_DIR}/cmake/shyft_epilogue.cmake)
