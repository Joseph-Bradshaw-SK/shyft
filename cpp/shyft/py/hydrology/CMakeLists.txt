

add_library(api SHARED
    api_actual_evapotranspiration.cpp     api_kirchner.cpp
    api_cell_environment.cpp              api_precipitation_correction.cpp
    api_priestley_taylor.cpp              api_gamma_snow.cpp
    api_pt_gs_k.cpp                       api_geo_cell_data.cpp
    api_region_environment.cpp            api_r_pm_gs_k.cpp
    api_routing.cpp                       api_cf_time.cpp
    api_glacier_melt.cpp                  api.cpp
    api_skaugen.cpp                       api_hbv_actual_evapotranspiration.cpp
    api_state.cpp                         api_kalman.cpp
    api_hbv_physical_snow.cpp             api_target_specification.cpp
    api_hbv_snow.cpp                      api_hbv_soil.cpp
    api_hbv_tank.cpp                      api_penman_monteith.cpp
    api_interpolation.cpp                 api_hydrology_vectors.cpp
    api_radiation.cpp                     drms.cpp
    api_snow_tiles.cpp                    api_r_pt_gs_k.cpp
    calibration_status.cpp                calibration_options.cpp
    gcd_service.cpp
)
target_include_directories(api PRIVATE ${python_include} ${python_numpy_include})
target_link_libraries(
    api
    PRIVATE shyft_private_flags shyft_core shyft_hydrology ${boost_py_link_libraries} ${python_lib}
    PUBLIC shyft_public_flags
)

set_target_properties(api PROPERTIES
    OUTPUT_NAME api
    PREFIX "_"
    SUFFIX ${py_ext_suffix}
    INSTALL_RPATH "$ORIGIN/../lib"
    VISIBILITY_INLINES_HIDDEN TRUE
)
install(TARGETS api ${shyft_runtime} DESTINATION ${SHYFT_PYTHON_DIR}/shyft/hydrology)


#
# define each shyft method-stack that exposes complete stacks
#
foreach(shyft_stack  "r_pt_gs_k" "r_pm_gs_k" "pt_gs_k" "pt_ss_k" "pt_hs_k" "pt_st_k" "hbv_stack" "pt_hps_k")
    add_library(${shyft_stack} SHARED  ${shyft_stack}.cpp)
    target_include_directories(${shyft_stack} PRIVATE ${python_include} ${python_numpy_include})
    set_target_properties(${shyft_stack} PROPERTIES
        OUTPUT_NAME ${shyft_stack}
        SUFFIX ${py_ext_suffix}
        PREFIX "_"
        INSTALL_RPATH "$ORIGIN/../../lib"
        VISIBILITY_INLINES_HIDDEN TRUE
    )
    target_link_libraries(
        ${shyft_stack}
        PRIVATE shyft_private_flags shyft_core shyft_hydrology ${boost_py_link_libraries} ${python_lib}
        PUBLIC shyft_public_flags
    )
    install(TARGETS ${shyft_stack} ${shyft_runtime} DESTINATION ${SHYFT_PYTHON_DIR}/shyft/hydrology/${shyft_stack})
endforeach(shyft_stack)

