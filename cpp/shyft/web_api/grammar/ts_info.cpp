#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {
         using shyft::time_series::ts_point_fx;

        inline ts_info mk_ts_info(string const&name,bool pfx,utctime dt,string const& tz_id,utcperiod dp, utctime tc,utctime tm) {
            return ts_info{name,(pfx?ts_point_fx::POINT_AVERAGE_VALUE:ts_point_fx::POINT_INSTANT_VALUE),dt,tz_id,dp,tc,tm};
        }

        template<typename Iterator,typename Skipper>
        ts_info_grammar<Iterator,Skipper>::ts_info_grammar():ts_info_grammar::base_type(start,"ts_info_grammar") {
           start = (
            lit('{')
            > lit("\"name\"")       >lit(':')  >quoted_string_   > lit(',')
            > lit("\"pfx\"")        >lit(':')  >bool_            > lit(',')
            > lit("\"delta_t\"")    >lit(':')  >time_            > lit(',')
            > lit("\"olson_tz_id\"")>lit(':')  >quoted_string_   > lit(',')
            > lit("\"data_period\"")>lit(':')  >period_          > lit(',')
            > lit("\"created\"")    >lit(':')  >time_            > lit(',')
            > lit("\"modified\"")   >lit(':')  >time_
            > lit('}')
            )[_val=phx::bind(mk_ts_info, _1,_2,_3,_4,_5,_6,_7)]
            ;
            on_error<fail>(start, error_handler(_4, _3, _2));
        }
        template struct ts_info_grammar<request_iterator_t,request_skipper_t>;


        template<typename Iterator,typename Skipper>
        ts_infos_grammar<Iterator,Skipper>::ts_infos_grammar():ts_infos_grammar::base_type(start,"ts_infos_grammar") {
           start = lit('[') > -(ts_info_ % lit(',') ) > lit(']');
           on_error<fail>(start, error_handler(_4, _3, _2));
        }

        template struct ts_infos_grammar<request_iterator_t,request_skipper_t>;
}
