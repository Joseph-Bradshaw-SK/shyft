import pytest

import numpy as np
from shyft.time_series import (TimeAxis, TsVector, TimeSeries,
                       DoubleVector, point_interpretation_policy, UtcTimeVector)
from shyft.dashboard.time_series.sources.ts_adapter import TsAdapter
from shyft.dashboard.time_series.state import State, Unit, Quantity
from shyft.dashboard.base.hashable import Hashable


@pytest.fixture
def test_ts_line():
    class TestTsAdapter(TsAdapter):
        def __init__(self, unit_to_decorate: Unit, point_interpretation: point_interpretation_policy=None) -> None:
            self.point_interpretation = point_interpretation or point_interpretation_policy.POINT_INSTANT_VALUE
            self.unit_to_decorate = unit_to_decorate

        def __call__(self, *, time_axis, unit) -> Quantity[TsVector]:
            vals = np.random.randn(len(time_axis.time_points[:-1]))
            ts = TimeSeries(time_axis, DoubleVector.from_numpy(vals),
                            self.point_interpretation)
            tsv = TsVector()
            tsv.extend([ts])
            return State.unit_registry.Quantity(tsv, self.unit_to_decorate)

    return TestTsAdapter(unit_to_decorate='MW')





@pytest.fixture
def mock_parent():
    class Parent:
        def __init__(self):
            self.count_data_update = 0
            self.count_time_axis_update = 0
            self.count_update_y_range = 0
            self.view_containers = []
            t = np.array([1, 2, 3, 4, 5, 6, 7, 8])
            self.time_axis = TimeAxis(UtcTimeVector.from_numpy(t))

        def trigger_data_update(self):
            """
            Mocking parent TsViewer
            """
            self.count_data_update += 1

        def trigger_time_axis_update(self):
            """
            Mocking parent Time Axis Handle
            """
            self.count_time_axis_update += 1

        def update_y_range(self):
            """
            Mocking parent updating y-range for ResetYRange tool
            """
            self.count_update_y_range += 1

        def add_view_container(self, obj):
            self.view_containers.append(obj)

        @property
        def view_time_axis(self):
            return self.time_axis

    return Parent()


@pytest.fixture
def mock_ts_vector():
    t = np.array([1, 2, 3, 4, 5, 6, 7, 8])
    d = np.array([10, 20, 30, 40, 50, 60, 70])
    time_axis = TimeAxis(UtcTimeVector.from_numpy(t))
    time_series = TimeSeries(time_axis, DoubleVector.from_numpy(d),
                                 point_interpretation_policy.POINT_AVERAGE_VALUE)
    ts_vector = TsVector([time_series])
    ts_vector = State.unit_registry.Quantity(ts_vector, 'MW')

    return ts_vector


@pytest.fixture
def mock_ds_view_handle_class():

    class DsViewHandle(Hashable):
        def __init__(self, tag):

            super().__init__()
            self.tag = tag

    return DsViewHandle




