#include <shyft/energy_market/stm/shop/shop_logger.h>
#include <ostream>

namespace shyft::energy_market::stm::shop {

shop_stream_logger::shop_stream_logger(std::string id, std::ostream& os) : id{ id }, os{ os } {}

void shop_stream_logger::info(const char* msg)  {
    os << "[" << id << "] [INFO] " << msg;
}
void shop_stream_logger::warning(const char* msg)  {
    os << "[" << id << "] [WARNING] " << msg;
}
void shop_stream_logger::error(const char* msg) {
    os << "[" << id << "] [ERROR] " << msg;
}
void shop_stream_logger::enter() {
    os << "[" << id << "] [ENTER]" << std::endl;
}
void shop_stream_logger::exit() {
    os << "[" << id << "] [EXIT]" << std::endl;
}

}