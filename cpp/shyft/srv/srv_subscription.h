#pragma once
#include <shyft/core/subscription.h>
#include <shyft/time/utctime_utilities.h>


namespace shyft::srv::subscription {
    using std::string;

    using shyft::core::subscription::manager_;
    using shyft::core::subscription::observable_;
    using shyft::core::subscription::observer_base;
    using shyft::core::utcperiod;

    enum class subscription_type : uint8_t {
        MODEL_INFOS,
        READ_MODEL
    };
    /** @brief observer class for srv::server models and model_infos **/
    struct srv_observer: observer_base {
        subscription_type st;
        vector<int64_t> mids;
        utcperiod per;

        srv_observer(manager_ const& sm, string const& request_id, vector<int64_t>const & mids, utcperiod per, subscription_type st):
            observer_base(sm, request_id), st{st}, mids{mids}, per{per} {
            observable_ subject;
            switch (st) {
                case subscription_type::MODEL_INFOS :
                    if (mids.size() > 0) {
                        vector<string> ids;
                        ids.reserve(mids.size());
                        for (auto mid : mids) {
                            ids.emplace_back("mid=" + std::to_string(mid));
                        }
                        terminals = sm->add_subscriptions(ids);
                    } else {
                        terminals = sm->add_subscription("model_infos");
                    }
                    break;
                case subscription_type::READ_MODEL :
                    if (mids.size() >= 1) {
                        terminals = sm->add_subscription("mid=" + std::to_string(mids[0]));
                    }
                    break;
            }
        }

        virtual bool recalculate() override {
            auto updated = has_changed();
            published_version = terminal_version();
            return updated;
        }
    };
}
