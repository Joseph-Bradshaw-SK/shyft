#pragma once
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/generators/json_struct.h>

namespace shyft::web_api::generator {

    using shyft::web_api::energy_market::attribute_value_type;
    /** 
     * @brief Emitter class(specialisation) for the boost variant containing
     * all employed types for proxy attributes
     *
     * @tparam OutputIterator
     */
    template<class OutputIterator>
    struct emit<OutputIterator, attribute_value_type> {
        emit(OutputIterator& oi, attribute_value_type const& pa) {
            boost::apply_visitor(emit_visitor(&oi), pa);
        }
    };
    x_emit_vec(attribute_value_type);


}
