#include <doctest/doctest.h>
#define NEEDS_OPTIMIZATION_COMMANDS
#include "model_simple.h"

// For debug output from tests:
// - define DEBUG_TESTS (uncomment)
// - modify debug flags within the "#ifdef DEBUG_TESTS" section only
// This will issue a message in the build output, to hopefully remember not to commit with debug flags enabled.
//#define DEBUG_TESTS
#ifdef DEBUG_TESTS
#pragma message("*** Remember to undefine DEBUG_TESTS before commit ***")
static const bool shop_console_output = false;
static const bool shop_log_files = false;
static const bool write_file_commands = false; // Should the test write log and results to file for manual inspection?
static const bool SKIP_OPTIMIZE = false;
#else
static const bool shop_console_output = false;
static const bool shop_log_files = false;
static const bool write_file_commands = false; // Should the test write log and results to file for manual inspection?
static const bool SKIP_OPTIMIZE = false;
#endif

using shyft::time_axis::generic_dt;

TEST_SUITE("stm_shop_optimize") {

    const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    const auto t_step = shyft::core::deltahours(1);
    const size_t n_step=18;
    const generic_dt time_axis{t_begin,t_step,n_step};
    
    const shyft::core::utcperiod t_period{ time_axis.total_period()};
    const auto t_end = t_period.end;

    static std::size_t run_id = 1; // Unique number, to not overwrite files from other tests when write_file_commands==true.

TEST_CASE("optimize simple model with defaults"
    * doctest::description("building and optmimizing simple model without explicit values for lrl/hrl/maxvol/p_min/p_max/p_nom")
    * doctest::skip(SKIP_OPTIMIZE))
{
    const bool always_inlet_tunnels = false;
    const bool use_defaults = true;
    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
}

TEST_CASE("optimize simple model without inlet"
    * doctest::description("building and optmimizing simple model without inlet segment: aggregate-penstock-maintunnel-reservoir")
    * doctest::skip(SKIP_OPTIMIZE))
{
    const bool always_inlet_tunnels = false;
    const bool use_defaults = false;
        
    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true); // Model with expected results
    // Run optimization
    // Alt 1: Fixed res
    shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
    // Alt 2: Dynamic res
    //auto n_steps = (size_t)((t_end - t_begin) / t_step);
    //shyft::time_axis::generic_dt ta{ t_begin, t_step, n_steps };
    //stm->optimize(ta, options);
    // Compare results:
    check_results(stm, rstm, t_begin, t_end, t_step);
}

TEST_CASE("optimize simple model with inlet"
    * doctest::description("building and optmimizing simple model with inlet segment: aggregate-inlet-penstock-maintunnel-reservoir")
    * doctest::skip(SKIP_OPTIMIZE))
{
    const bool always_inlet_tunnels = false;
    const bool use_defaults = false;
    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1);
    auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true);
    shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
    check_results(stm, rstm, t_begin, t_end, t_step);
}

TEST_CASE("optimize simple model with discharge group"
          * doctest::description("building and optmimizing simple model without explicit values for lrl/hrl/maxvol/p_min/p_max/p_nom")
          * doctest::skip(SKIP_OPTIMIZE))
{
    using namespace shyft::energy_market;
    const bool always_inlet_tunnels = false;
    const bool use_defaults = true;
    bool verbose=false;// true to view ouput from operational reserve  run etc.

    // Build models
    auto stm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto rstm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);

    // Fetch stm objects connected to shop objects
    auto rsv_ = std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(stm->hps.front()->find_reservoir_by_name("reservoir"));
    auto unit_ = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(stm->hps.front()->find_unit_by_name("aggregate"));
    auto flood_river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(stm->hps.front()->find_waterway_by_name("waterroute flood river"));
    auto bypass_river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(stm->hps.front()->find_waterway_by_name("waterroute bypass"));
    auto bypass_gate_ = std::dynamic_pointer_cast<shyft::energy_market::stm::gate>(bypass_river_->gates.front());
    auto river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(stm->hps.front()->find_waterway_by_name("waterroute river"));

    // Decorate connection with attrs to enforce discharge group
    river_->discharge.reference = make_constant_ts(t_begin, t_end, 0);
    river_->discharge.constraint.accumulated_max = make_constant_ts(t_begin, t_end, 1e6);
    river_->discharge.penalty.cost.accumulated_max = make_constant_ts(t_begin, t_end, 10);
    //
    // Set buypass schedule, to check if river_ gets input from both production and bypass
    bypass_gate_->discharge.schedule = make_constant_ts(t_begin, t_end, 10);

    generic_dt time_axis{t_begin,t_step,n_step};
    generic_dt time_axis_accumulate{t_begin,t_step,n_step + 1};
    shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
    shyft::energy_market::stm::shop::shop_system::optimize(*rstm, time_axis, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
    check_results_with_discharge_group(stm, rstm, t_begin, t_end, t_step);

    // Helper lambda to print results
    auto show_ts=[verbose](string title,apoint_ts const&a,double scale, std::ostream&os){
        if(!verbose) return;
        os<<title;
        if(!!a) {
            for(auto i=0u;i<a.size();++i) {
                if(i) os<<", ";
                os.width(4);os.precision(0);
                os<<std::fixed<< a.value(i)/scale;
            }
        } else os<<"<empty>";
        os<<std::endl;
    };

    // Observer river_ object, connecting bypass and wtr from unit, contributes to discharge.result
    show_ts(rsv_->name+".volume.result=",rsv_->volume.result,1e6,std::cout);
    show_ts(rsv_->name+".level.result=",rsv_->level.result,1,std::cout);
    show_ts(unit_->name+".production.result=",unit_->production.result, 1,std::cout);
    show_ts(unit_->name+".discharge.result=",unit_->discharge.result, 1,std::cout);
    show_ts(unit_->name+".discharge.result.accumulate=",unit_->discharge.result.accumulate(time_axis_accumulate), 1,std::cout);
    show_ts(flood_river_->name+".discharge.result=",flood_river_->discharge.result,1,std::cout);
    show_ts(bypass_river_->name+".discharge.result=",bypass_river_->discharge.result,1,std::cout);
    show_ts(bypass_gate_->name+".discharge.result=",bypass_gate_->discharge.result,1,std::cout);
    show_ts(river_->name+".discharge.result=",river_->discharge.result,1,std::cout);
    show_ts(river_->name+".discharge.result.accumulate=",river_->discharge.result.accumulate(time_axis_accumulate),1,std::cout);

}

TEST_CASE("optimize_model_w_3_units_and_reserves"
    * doctest::description("building and optmimizing model with 3 units")
    * doctest::skip(SKIP_OPTIMIZE))
{
    using namespace shyft::energy_market;
    const bool always_inlet_tunnels = false;
    const bool use_defaults = false;
    auto stm = build_simple_model_n_units_with_reserves(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, 3);
    auto log_handler = std::make_shared<shyft::energy_market::stm::shop::shop_memory_logger>();

    shyft::energy_market::stm::shop::shop_system shop{ time_axis };
    shop.install_logger(log_handler);
    shop.set_logging_to_stdstreams(shop_console_output);
    shop.set_logging_to_files(shop_log_files);
    shop.emit(*stm);
    auto cmds=optimization_commands(run_id, write_file_commands);
    cmds.insert(cmds.begin(),stm::shop::shop_command::set_universal_mip_on());// let shop find the ideal combination
    shop.command(cmds);
    shop.collect(*stm);
    shop.complete(*stm);
    for(auto &wx: stm->hps.front()->waterways){
        auto w=std::dynamic_pointer_cast<stm::waterway>(wx);
        if(w) {
            CHECK_UNARY(w->discharge.result.size()>0);// verify all water way discharges are filled (done by shop.update call above)
        }
    }
    bool verbose=true;// true to view ouput from operational reserve run etc.
    if (verbose) {
        std::cout<<"Topology (graphviz):\n\n";
        shop.export_topology(false, false, std::cout);
    }
    if(verbose) std::cout<<std::endl<<"Timeseries:\n\n";
    auto show_ts=[verbose](string title,apoint_ts const&a,double scale, std::ostream&os, bool force_verbose=false){
      if(!verbose && !force_verbose) return;
      os.width(45);
      os<<std::left<<title<<std::right;
      if(!!a) {
        for(auto i=0u;i<a.size();++i) {
            if(i) os<<", ";
            os.width(4);os.precision(0);
            os<<std::fixed<<a.value(i)/scale;
        }
      } else os<<"<empty>";
      os<<"\n";
    };
    apoint_ts run_ts(time_axis,1.0,shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    auto m=stm->market.front();
    show_ts("market "+m->name +":MW",m->load.use_time_axis_from(run_ts),1e6,std::cout);
    for(auto const & u_:stm->hps.front()->units) {
        auto u=std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(u_);
        show_ts("unit "+u->name +".max:MW",u->production.static_max.use_time_axis_from(run_ts),1e6,std::cout);
        show_ts("unit "+u->name +".result:MW",u->production.result,1e6,std::cout);
    }
    for(auto const & u_:stm->hps.front()->units) {
        auto u=std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(u_);
        show_ts(u->name+".fcr_n.up",u->reserve.fcr_n.up.result,1e6,std::cout);
        show_ts(u->name+".fcr_n.dn",u->reserve.fcr_n.down.result,1e6,std::cout);
        show_ts(u->name+".afrr.up",u->reserve.afrr.up.result,1e6,std::cout);
        show_ts(u->name+".afrr.dn",u->reserve.afrr.down.result,1e6,std::cout);
        auto mbr_ts= compute_unit_group_membership_ts(stm->unit_groups,u,run_ts,shyft::energy_market::stm::unit_group_type::fcr_n_up);
        show_ts(u->name+".fcr_n.up.unit_group",mbr_ts,1.0,std::cout);
        auto afrr_up_ts= compute_unit_group_membership_ts(stm->unit_groups,u,run_ts,shyft::energy_market::stm::unit_group_type::afrr_up);
        show_ts(u->name+".afrr.up.unit_group",afrr_up_ts,1.0,std::cout);
    }
    for(auto const &ug: stm->unit_groups) {
        if (ug->get_energy_market_area())
            continue; // skip energy market area groups, only interested in reserves here
        show_ts(ug->name+".obligation.schedule",!ug->obligation.schedule ? ug->obligation.schedule : ug->obligation.schedule.use_time_axis_from(run_ts),1e6,std::cout);
        show_ts(ug->name+".obligation.result",ug->obligation.result,1e6,std::cout); // Note: This is currently read from reserve slack attribute in shop, representing resulting slack when deviating above the obligation.
        show_ts(ug->name+".obligation.penalty",ug->obligation.penalty,1e6,std::cout);
    }
    for(auto const &r_: stm->hps.front()->reservoirs) {
        auto r=std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(r_);
        show_ts(r->name+".volume.result",r->volume.result, 1e6, std::cout);
        show_ts(r->name+".water_value.result.local_energy",r->water_value.result.local_energy, 1, std::cout);
        show_ts(r->name+".water_value.result.local_volume",r->water_value.result.local_volume, 1, std::cout);
        show_ts(r->name+".water_value.result.global_volume",r->water_value.result.global_volume,1, std::cout);
    }

    if(verbose) std::cout<<std::endl<<"Shop log:\n\n";
    int regression_severity_sum=0;
    for(auto const &msg:log_handler->messages) {
        regression_severity_sum += msg.severity;
        if(verbose) std::cout<<msg.severity<<":"<<msg.message;
    }
    if(verbose) std::cout<<std::endl;
    FAST_CHECK_EQ(regression_severity_sum,6*shyft::energy_market::stm::shop::shop_log_entry::log_severity::warning);// on a fresh case, this is number of warnings

    REQUIRE(log_handler->messages.size() > 0);

    // Verify reserve results: Sum of unit group schedules vs unit results for each reserve type.
    // Assuming the schedule will be followed 100% in correct test, not sure if this will always be true.
    REQUIRE_EQ(stm->unit_groups.size(), 4);
    REQUIRE_EQ(stm->hps.front()->units.size(), 3);
    int reserve_types[]{ stm::unit_group_type::fcr_n_up, stm::unit_group_type::afrr_up };
    for (int reserve_type : reserve_types) {
        apoint_ts schedule{time_axis,0.0,shyft::time_series::POINT_AVERAGE_VALUE};
        for(auto const &ug: stm->unit_groups) {
            if (ug->group_type == reserve_type) {
                schedule = schedule + ug->obligation.schedule;
            }
        }
        apoint_ts result{time_axis,0.0,shyft::time_series::POINT_AVERAGE_VALUE};
        for(auto const& u_:stm->hps.front()->units) {
            auto u=std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(u_);
            if (reserve_type == stm::unit_group_type::fcr_n_up) {
                result = result + u->reserve.fcr_n.up.result;
            } else if (reserve_type == stm::unit_group_type::afrr_up) {
                result = result + u->reserve.afrr.up.result;
            }
        }
        if (schedule != result) {
            string reserve_type_name;
            switch(reserve_type){
                case stm::unit_group_type::fcr_n_up: reserve_type_name = "fcr_n_up"; break;
                case stm::unit_group_type::afrr_up: reserve_type_name = "afrr_up"; break;
            }
            CHECK_MESSAGE(false, (string{"Sum of unit reserves for "} + reserve_type_name + string{" does not match the schedule"}));
            show_ts(reserve_type_name+" sum unit group schedule",schedule,1e6,std::cout,true);
            show_ts(reserve_type_name+" sum unit result",result,1e6,std::cout,true);
        }
    }
}

TEST_CASE("optimize_multi_market"
    * doctest::description("multimarket demand/supply 3 units")
    * doctest::skip(SKIP_OPTIMIZE))
{
    using namespace shyft::energy_market;
    using xy_point=shyft::energy_market::hydro_power::point;
    using xy_points=shyft::energy_market::hydro_power::xy_point_curve;
    using shyft::energy_market::hydro_power::xy_point_curve_;
    using stm::t_xy_;
    using std::vector;
    using std::make_shared;

    const bool always_inlet_tunnels = false;
    const bool use_defaults = false;
    auto stm = build_simple_model_n_units_with_reserves(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, 3);
    // modify the system to use demand/supply tables:
    const double mega=1e6;
    auto ema=stm->market[0];
    ema->demand.bids=make_shared<t_xy_::element_type>( // shop sale opportunities part
            t_xy_::element_type{ // // x=money, y=W
                {time_axis.time(0)     ,make_shared<xy_points>(vector<xy_point>{{48.0/mega,10*mega}})},
                {time_axis.time(6)     ,make_shared<xy_points>(vector<xy_point>{{40.0/mega,220.0*mega}})},
                {time_axis.time(7)     ,make_shared<xy_points>(vector<xy_point>{{30.0/mega,120.0*mega},{50.0/mega,80.0*mega},{55.0/mega,20.0*mega}})},
                {time_axis.time(8)     ,make_shared<xy_points>(vector<xy_point>{{30.0/mega,220.0*mega}})},
                {time_axis.time(12)    ,make_shared<xy_points>(vector<xy_point>{{48.0/mega,220.0*mega}})},
                {t_end                 ,make_shared<xy_points>(vector<xy_point>{{35.0/mega,220.0*mega}})},
            }
        );
    const double buy=1.0; // set 0 to close shop ability to buy,
    ema->supply.bids=make_shared<t_xy_::element_type>( // shop buy oppportunities part
            t_xy_::element_type{ // x=money, y=W
                {time_axis.time(0)     ,make_shared<xy_points>(vector<xy_point>{{148.0/mega,buy*10*mega}})},
                {time_axis.time(1)     ,make_shared<xy_points>(vector<xy_point>{{10.0/mega,buy*5*mega},{15.0/mega,buy*20*mega} })},
                {time_axis.time(2)     ,make_shared<xy_points>(vector<xy_point>{{50.0/mega,buy*10*mega}})},
                {time_axis.time(6)     ,make_shared<xy_points>(vector<xy_point>{{45.0/mega,buy*30.0*mega}})},
                {time_axis.time(7)     ,make_shared<xy_points>(vector<xy_point>{{37.0/mega,buy*30.0*mega}})},
                {time_axis.time(12)    ,make_shared<xy_points>(vector<xy_point>{{50.0/mega,buy*30.0*mega}})},
                {t_end                 ,make_shared<xy_points>(vector<xy_point>{{36.0/mega,buy*30.0*mega}})},
            }
        );
    // close primary market object sale/buy parts. zero price, keep load
    ema->max_buy=apoint_ts{time_axis,0.0,POINT_AVERAGE_VALUE};
    ema->max_sale=apoint_ts{time_axis,0.0,POINT_AVERAGE_VALUE};
    //ema->price=apoint_ts{time_axis,0.0,POINT_AVERAGE_VALUE};
    // put into place droop steps, to ensure we trigger that functionality in shop
    for(auto const & u_:stm->hps.front()->units) {
        auto u=std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(u_);
        u->reserve.droop_steps=make_shared<t_xy_::element_type> (
            t_xy_::element_type{ // x=static setting number, y= static setting in % (1..100)
                {time_axis.time(0), make_shared<xy_points>( vector<xy_point>{{1,4},{2,6},{3,8},{4,10}} )}
            }
        );
    }
    auto log_handler = std::make_shared<shyft::energy_market::stm::shop::shop_memory_logger>();
    // modify the market, so that we have
    shyft::energy_market::stm::shop::shop_system shop{ time_axis };
    shop.install_logger(log_handler);
    shop.set_logging_to_stdstreams(shop_console_output);
    shop.set_logging_to_files(shop_log_files);
    shop.emit(*stm);
    using namespace shyft::energy_market::stm::shop;
    std::vector<shop_command> cmds{
        shop_command::set_universal_mip_on(),
        //unknown command exception shop_command::set_mc_nseg_all(4),
        shop_command::set_droop_discretization_limit(6.0),//verify it works, needs license, otherwise it will fail.
        shop_command::set_method_primal(),
        shop_command::set_code_full(),
        shop_command::start_sim(3),
        shop_command::set_code_incremental(),
        shop_command::start_sim(3),
        //shop_command{"print mc_curves mc_curves.xml 0 10 /up /down"}
        shop_command{"save case multimarket.txt"}

    };

    shop_global_settings globs=shop.get_global_settings();
    auto lpen=globs.load_penalty_flag.get();
    auto lpenv=globs.load_penalty_cost.get();
    auto g= globs.gravity.get();
    MESSAGE("globs values "<<lpen<<","<<lpenv<<",g="<<g);
    shop.command(cmds);
    shop.collect(*stm);

    bool verbose=true;// true to view ouput from operational reserve run etc.
    if (verbose) {
        std::cout<<"Topology (graphviz):\n\n";
        shop.export_topology(false, false, std::cout);
    }
    if(verbose) std::cout<<std::endl<<"Timeseries:\n\n";
    auto show_ts=[verbose](string title,apoint_ts const&a,double scale, std::ostream&os, bool force_verbose=false){
      if(!verbose && !force_verbose) return;
      os.width(45);
      os<<std::left<<title<<std::right;
      if(!!a) {
        for(auto i=0u;i<a.size();++i) {
            if(i) os<<", ";
            os.width(4);os.precision(0);
            os<<std::fixed<<a.value(i)/scale;
        }
      } else os<<"<empty>";
      os<<"\n";
    };
    apoint_ts run_ts(time_axis,1.0,shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    auto m=stm->market.front();
    show_ts("m.load "+m->name +":MW",m->load.use_time_axis_from(run_ts),1e6,std::cout);
    show_ts("m.buy  "+m->name +":MW",m->buy.use_time_axis_from(run_ts),1e6,std::cout);
    show_ts("m.sale "+m->name +":MW",m->sale.use_time_axis_from(run_ts),-1e6,std::cout);
    show_ts("m.demand.usage "+m->name +":MW",m->demand.usage.result.use_time_axis_from(run_ts),1e6,std::cout);
    show_ts("m.demand.price "+m->name +":EUR",m->demand.price.result.use_time_axis_from(run_ts),1e-6,std::cout);
    show_ts("m.supply.usage "+m->name +":MW",m->supply.usage.result.use_time_axis_from(run_ts),1e6,std::cout);
    show_ts("m.supply.price "+m->name +":EUR",m->supply.price.result.use_time_axis_from(run_ts),1e-6,std::cout);
    for(auto const & u_:stm->hps.front()->units) {
        auto u=std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(u_);
        //show_ts("unit "+u->name +".max:MW",u->production.static_max.use_time_axis_from(run_ts),1e6,std::cout);
        show_ts("unit "+u->name +".result:MW",u->production.result,1e6,std::cout);
    }
#if 1
    for(auto const & u_:stm->hps.front()->units) {
        auto u=std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(u_);
        show_ts(u->name+".fcr_n.up",u->reserve.fcr_n.up.result,1e6,std::cout);
        show_ts(u->name+".fcr_n.dn",u->reserve.fcr_n.down.result,1e6,std::cout);
        show_ts(u->name+".afrr.up",u->reserve.afrr.up.result,1e6,std::cout);
        show_ts(u->name+".afrr.dn",u->reserve.afrr.down.result,1e6,std::cout);
        auto mbr_ts= compute_unit_group_membership_ts(stm->unit_groups,u,run_ts,shyft::energy_market::stm::unit_group_type::fcr_n_up);
        show_ts(u->name+".fcr_n.up.unit_group",mbr_ts,1.0,std::cout);
        auto afrr_up_ts= compute_unit_group_membership_ts(stm->unit_groups,u,run_ts,shyft::energy_market::stm::unit_group_type::afrr_up);
        show_ts(u->name+".afrr.up.unit_group",afrr_up_ts,1.0,std::cout);
    }
    for(auto const &ug: stm->unit_groups) {
        if (ug->get_energy_market_area())
            continue; // skip energy market area groups, only interested in reserves here
        show_ts(ug->name+".obligation.schedule",!ug->obligation.schedule ? ug->obligation.schedule : ug->obligation.schedule.use_time_axis_from(run_ts),1e6,std::cout);
        show_ts(ug->name+".obligation.result",ug->obligation.result,1e6,std::cout); // Note: This is currently read from reserve slack attribute in shop, representing resulting slack when deviating above the obligation.
        show_ts(ug->name+".obligation.penalty",ug->obligation.penalty,1e6,std::cout);
    }
    for(auto const &r_: stm->hps.front()->reservoirs) {
        auto r=std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(r_);
        show_ts(r->name+".volume.result",r->volume.result, 1e6, std::cout);
        show_ts(r->name+".water_value.result.local_energy",r->water_value.result.local_energy, 1, std::cout);
        show_ts(r->name+".water_value.result.local_volume",r->water_value.result.local_volume, 1, std::cout);
        show_ts(r->name+".water_value.result.global_volume",r->water_value.result.global_volume,1, std::cout);
    }
#endif

    if(verbose) std::cout<<std::endl<<"Shop log:\n\n";
    int regression_severity_sum=0;
    for(auto const &msg:log_handler->messages) {
        regression_severity_sum += msg.severity;
        if(verbose) std::cout<<msg.severity<<":"<<msg.message;
    }
    if(verbose) std::cout<<std::endl;
    FAST_CHECK_EQ(regression_severity_sum,6*shyft::energy_market::stm::shop::shop_log_entry::log_severity::warning);// on a fresh case, this is number of warnings

    REQUIRE(log_handler->messages.size() > 0);

    // Verify reserve results: Sum of unit group schedules vs unit results for each reserve type.
    // Assuming the schedule will be followed 100% in correct test, not sure if this will always be true.
    REQUIRE_EQ(stm->unit_groups.size(), 4);
    REQUIRE_EQ(stm->hps.front()->units.size(), 3);
    int reserve_types[]{ stm::unit_group_type::fcr_n_up, stm::unit_group_type::afrr_up };
    for (int reserve_type : reserve_types) {
        apoint_ts schedule{time_axis,0.0,shyft::time_series::POINT_AVERAGE_VALUE};
        for(auto const &ug: stm->unit_groups) {
            if (ug->group_type == reserve_type) {
                schedule = schedule + ug->obligation.schedule;
            }
        }
        apoint_ts result{time_axis,0.0,shyft::time_series::POINT_AVERAGE_VALUE};
        for(auto const& u_:stm->hps.front()->units) {
            auto u=std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(u_);
            if (reserve_type == stm::unit_group_type::fcr_n_up) {
                result = result + u->reserve.fcr_n.up.result;
            } else if (reserve_type == stm::unit_group_type::afrr_up) {
                result = result + u->reserve.afrr.up.result;
            }
        }
        if (schedule != result) {
            string reserve_type_name;
            switch(reserve_type){
                case stm::unit_group_type::fcr_n_up: reserve_type_name = "fcr_n_up"; break;
                case stm::unit_group_type::afrr_up: reserve_type_name = "afrr_up"; break;
            }
            CHECK_MESSAGE(false, (string{"Sum of unit reserves for "} + reserve_type_name + string{" does not match the schedule"}));
            show_ts(reserve_type_name+" sum unit group schedule",schedule,1e6,std::cout,true);
            show_ts(reserve_type_name+" sum unit result",result,1e6,std::cout,true);
        }
    }
}

TEST_CASE("optimize_model_unit_group_market_area_reference")
{
    // TODO: Implement
}

TEST_CASE("optimize with log handler"
    * doctest::description("building and optmimizing simple model with log handler attached")
    * doctest::skip(SKIP_OPTIMIZE))
{
    const bool always_inlet_tunnels = false;
    const bool use_defaults = true;
    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);

    //struct test_stm_shop_log_handler : shop_stream_logger {
    //	//std::ostringstream stream;
    //	test_stm_shop_log_handler() : shop_stream_logger{ "test_stm_shop_log_handler", std::cout } { enter(); }
    //	~test_stm_shop_log_handler() { exit(); }
    //};
    //auto log_handler = std::make_shared<test_stm_shop_log_handler>();

    auto log_handler = std::make_shared<shyft::energy_market::stm::shop::shop_memory_logger>();

    //shyft::energy_market::stm::shop::shop_system::optimize(stm, t_begin, t_end, t_step, optimization_commands(run_id, write_file_commands), silent);

    shyft::energy_market::stm::shop::shop_system shop{ time_axis };
    shop.install_logger(log_handler);
    shop.set_logging_to_stdstreams(shop_console_output);
    shop.set_logging_to_files(shop_log_files);
    shop.emit(*stm);
    shop.command(optimization_commands(run_id, write_file_commands));
    shop.collect(*stm);
    //shop.export_topology(false, false, std::cout);

#if 0
    // For debugging:
    size_t i = 0;
    for (const auto& msg : log_handler->messages) {
        std::cout << i++ << " [" << (int)msg.severity << "]:" << msg.message;
    }
#endif

    REQUIRE(log_handler->messages.size() > 100 );
}

}
